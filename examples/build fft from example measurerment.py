from pytimbre.audio_files.calibrated_binary_files import CalibratedBinaryFile, MeasurementLogFile
from pytimbre.spectral.spectra import SpectrumByFFT
import matplotlib.pyplot as plt

"""
This script will explore how to load a file created by the AFRL LabVIEW Acoustic Recording Measurement System (
ALARMS) that consists of a measurement log file and a calibrated binary file containing the audio data. This waveform 
will then pass through the spectral creation classes to produce a visual of the spectral levels.
"""

#   Create the MeasurementLogFile
log = MeasurementLogFile("../tests/Test Data/audio_files/Files/AFRL Acoustic Recorder/ID00110_AFRL_log.txt")

#   Now, using this log file, read the binary audio data
wfm = CalibratedBinaryFile(log, "../tests/Test Data/audio_files/Files/AFRL Acoustic Recorder/ID00110_00001.AFRL.bin")

#   This is the minimal information required then to generate the spectral levels. So let's start with the narrowband
#   representation.
spectrum = SpectrumByFFT(wfm)

#   Plot the data
fig, ax = plt.subplots()
ax.semilogx(spectrum.frequencies, spectrum.pressures_decibels)
plt.pause(5)

#   Create a new spectrum that is fractional octave
tob_spectrum = spectrum.to_fractional_octave_band(3)

ax.clear()
ax.semilogx(tob_spectrum.frequencies, tob_spectrum.pressures_decibels)

plt.pause(5)
