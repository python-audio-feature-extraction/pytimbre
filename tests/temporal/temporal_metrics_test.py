from unittest import TestCase
import datetime
import math
from pytimbre.temporal.temporal_metrics import TemporalMetrics, EquivalentLevel
from pytimbre.waveform import Waveform, WeightingFunctions, LeqDurationMode


class TestTemporalMetrics(TestCase):
    def test_sound_exposure_level(self):
        y = []
        x = []
        max = int(20 / 0.1)
        for index in range(0, max, 1):
            input = index * 0.1
            x.append(datetime.datetime.min + datetime.timedelta(seconds=input))
            y.append(100 * math.exp(-math.pow(input - 10.0, 2.0) / math.sqrt(100)))
        self.assertAlmostEqual(114.09, TemporalMetrics.sound_exposure_level(x, y, 10), places=2)

    def test_find_dB_down_limits(self):
        y = []
        max = int(20 / 0.1)
        for index in range(0, max, 1):
            input = index * 0.1
            y.append(100 * math.exp(-math.pow(input - 10.0, 2.0) / math.sqrt(2)))
        start, stop = TemporalMetrics.find_decibel_down_limits(y, 10)
        self.assertEqual(96, start)
        self.assertEqual(104, stop)
        y = []
        for index in range(0, max, 1):
            input = index * 0.1
            y.append(100 * math.exp(-math.pow(input - 10.0, 2.0) / math.sqrt(500)))
        start, stop = TemporalMetrics.find_decibel_down_limits(y, 10)
        self.assertEqual(84, start)
        self.assertEqual(116, stop)

    def test_leq_convert_duration(self):
        level_out = TemporalMetrics.leq_convert_duration(85.0, tin=4.0*3600, tout=8.0*3600)
        self.assertAlmostEqual(85.0 - 3, level_out, delta=0.1)

    def test_leq(self):
        spl = [50, 55, 55, 50]
        self.assertAlmostEqual(53.18, TemporalMetrics.leq(spl, 0.25, 1, 0, 3), places=2)
        spl = [50, 50, 50, 50]
        self.assertAlmostEqual(50, TemporalMetrics.leq(spl, 0.25, 1, 0, 3), places=2)

    def test_if_equivalent_level_returns_correct_magnitudes(self):
        times = [0, 1, 2, 3]
        overall_level_hist_steady_state = [94, 94, 94, 94]
        overall_level_hist_transient = [14, 94, 14, 94]

        leq_steady_state = TemporalMetrics.equivalent_level(
            times,
            overall_level_hist_steady_state,
            leq_mode=LeqDurationMode.steady_state,
            equivalent_duration=4.0,
            exposure_duration=4.0
        )
        self.assertAlmostEqual(94.0, leq_steady_state, delta=0.1)

        leq_steady_state2 = TemporalMetrics.equivalent_level(
            times,
            overall_level_hist_steady_state,
            leq_mode=LeqDurationMode.steady_state,
            equivalent_duration=8.0,
            exposure_duration=4.0
        )
        self.assertAlmostEqual(94.0 - 3, leq_steady_state2, delta=0.1)

        leq_steady_state3 = TemporalMetrics.equivalent_level(
            times,
            overall_level_hist_steady_state,
            leq_mode=LeqDurationMode.steady_state,
            equivalent_duration=4.0,
            exposure_duration=8.0
        )
        self.assertAlmostEqual(94.0 + 3, leq_steady_state3, delta=0.1)

        leq_transient = TemporalMetrics.equivalent_level(
            times,
            overall_level_hist_transient,
            leq_mode=LeqDurationMode.transient,
            equivalent_duration=4.0,
        )
        self.assertAlmostEqual(94.0 - 3, leq_transient, delta=0.1)

        leq_transient = TemporalMetrics.equivalent_level(
            times,
            overall_level_hist_transient,
            leq_mode=LeqDurationMode.transient,
            equivalent_duration=4.0 * 2,
        )
        self.assertAlmostEqual(94.0 - 3 - 3, leq_transient, delta=0.1)

    def test_equivalent_level_trimming(self):
        times = [0, 1, 2, 3]
        overall_level_hist_steady_state = [94, 94, 94, 94]
        overall_level_hist_transient = [14, 94, 14, 94]

        leq_steady_state = TemporalMetrics.equivalent_level(
            times,
            overall_level_hist_steady_state,
            leq_mode=LeqDurationMode.steady_state,
            equivalent_duration=4.0,
            exposure_duration=4.0,
            start_sample=0,
            stop_sample=1
        )
        self.assertAlmostEqual(94.0, leq_steady_state, delta=0.1)

        leq_transient = TemporalMetrics.equivalent_level(
            times,
            overall_level_hist_transient,
            leq_mode=LeqDurationMode.transient,
            equivalent_duration=4.0,
            start_sample=0,
            stop_sample=1
        )
        self.assertAlmostEqual(94.0 - 3 - 3, leq_transient, delta=0.1)

    def test_rt60_calculation(self):
        wfm = Waveform.generate_tone()

        rt60 = 0.2

        self.assertAlmostEqual(rt60, TemporalMetrics.estimate_RT60(wfm), delta=1e-1)


class TestEquivalentLevel(TestCase):
    def test_constructor(self):
        leq = EquivalentLevel(
            equivalent_pressure_decibels=85,
            equivalent_duration=datetime.timedelta(hours=8)
        )
        self.assertEqual(leq.equivalent_pressure_decibels, 85.0)
        self.assertEqual(leq.equivalent_duration.total_seconds(), 8 * 60 * 60)
        self.assertIsInstance(leq.weighting, WeightingFunctions)

    def test_sel(self):
        leq = EquivalentLevel(
            equivalent_pressure_decibels=85,
            equivalent_duration=datetime.timedelta(hours=8)
        )
        self.assertAlmostEqual(129.6, leq.sel, delta=0.1)

    def test_leq8hr(self):
        leq = EquivalentLevel(
            equivalent_pressure_decibels=85,
            equivalent_duration=datetime.timedelta(hours=4)
        )
        self.assertAlmostEqual(85.0 - 3, leq.leq8hr, delta=0.1)

    def test_noise_dose_pct(self):
        leq = EquivalentLevel(
            equivalent_pressure_decibels=85,
            equivalent_duration=datetime.timedelta(hours=4),
            weighting=WeightingFunctions.a_weighted
        )
        self.assertAlmostEqual(leq.noise_dose_pct, 50., delta=1)

        leq = EquivalentLevel(
            equivalent_pressure_decibels=85,
            equivalent_duration=datetime.timedelta(hours=8),
            weighting=WeightingFunctions.a_weighted
        )
        self.assertAlmostEqual(leq.noise_dose_pct, 100., delta=1)

    def test_leq_convert_duration(self):
        self.assertAlmostEqual(EquivalentLevel.leq_convert_duration(85, 1, 1), 85, delta=0.1)
        self.assertAlmostEqual(EquivalentLevel.leq_convert_duration(129.6, 1, 8 * 60 * 60), 85, delta=0.1)


