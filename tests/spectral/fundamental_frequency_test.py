import unittest
import numpy as np
from pytimbre.spectral.fractional_octave_band import FractionalOctaveBandTools as fob
from pytimbre.spectral.spectra import Spectrum
from pytimbre.spectral.fundamental_frequency import FundamentalFrequencyCalculator


class TestFundamentalFrequency(unittest.TestCase):
    def test_Swipe_increase_decrease(self):
        spl = 95 - np.arange(0, 31, 1)
        spl[0] = 100

        spec = Spectrum()
        spec.frequencies = fob.tob_frequencies()
        spec.pressures_decibels = spl

        f0 = FundamentalFrequencyCalculator()
        fundamental = f0.fundamental_swipe(spec)
        self.assertAlmostEqual(fob.tob_frequencies()[0], fundamental)

        spec.pressures_decibels = spl[::-1]

        fundamental = f0.fundamental_swipe(spec)
        self.assertAlmostEqual(fob.tob_frequencies()[-1], fundamental)

    def test_swipe_gauss_peak(self):
        spl = np.array([25, 25, 25, 25, 25, 25, 25, 25, 25, 25, 30, 55, 65, 95, 100, 95, 65, 55, 30, 25, 25, 25, 25,
                        25, 25, 25, 25, 25, 25, 25, 25])

        spec = Spectrum()
        spec.frequencies = fob.tob_frequencies()
        spec.pressures_decibels = spl

        f0 = FundamentalFrequencyCalculator()
        fundamental = f0.fundamental_swipe(spec)
        self.assertAlmostEqual(250, fundamental, delta=5)

    def test_swipe_mambo(self):
        spl = np.array(
            [43.81180328,    46.89868852, 46.42377049, 47.89754098, 49.62606557, 50.17213115,
                46.9657377,  46.99032787, 51.1152459,  51.49819672, 47.94819672, 49.18295082, 46.24163934, 44.61327869,
                41.70885246, 40.96098361, 35.06508197, 46.75557377, 52.37557377, 41.23377049, 40.68721311, 45.25786885,
                42.21229508, 41.19737705, 42.23836066, 40.64360656, 39.1752459,  40.44131148, 44.30557377, 45.8204918,
             44.17]
            )

        spec = Spectrum()
        spec.frequencies = fob.tob_frequencies()
        spec.pressures_decibels = spl

        f0 = FundamentalFrequencyCalculator()
        fundamental = f0.fundamental_swipe(spec)
        self.assertAlmostEqual(629.96, fundamental, delta=9)

    def test_swipe_harmonic(self):
        sample_rate = 48000
        nfft = 4096
        dsf = np.arange(nfft) * sample_rate / nfft
        f = dsf[:2048]

        spl = 25.5 * np.ones((2048,))
        spl[500] = 100
        spl[1000] = 80
        spl[2000] = 60

        spec = Spectrum()
        spec.frequencies = f
        spec.pressures_decibels = spl

        f0 = FundamentalFrequencyCalculator()
        fundamental = f0.fundamental_swipe(spec)
        self.assertAlmostEqual(f[500], fundamental, delta=9)
