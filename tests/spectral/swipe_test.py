from pytimbre.spectral.swipe import swipe_spectral_estimation, swipe
from pytimbre.waveform import Waveform
from pytimbre.audio_files.wavefile import WaveFile
from pytimbre.spectral.spectra import SpectrumByFFT
import unittest
from pathlib import Path


class TestSwipe(unittest.TestCase):
    def test_swipe_spectral_estimation(self):
        wfm = Waveform.generate_tone(1000)
        spectrum = SpectrumByFFT(wfm, fft_size=4096*2)
        f0 = swipe_spectral_estimation(spectrum, pitch_resolution=1/80)[0]

        self.assertAlmostEqual(1000, f0, delta=15)

        wfm = WaveFile(str(Path(__file__).parents[1]) +
                       "/Test Data/audio_files/multi_tone_simulated_noise_600hz_f0.wav")

        spectrum = SpectrumByFFT(wfm, fft_size=4096 * 2)
        f0 = swipe_spectral_estimation(spectrum, pitch_resolution=1 / 80)[0]

        self.assertAlmostEqual(600, f0, delta=15)

        wfm = WaveFile(str(Path(__file__).parents[1]) +
                       "/Test Data/audio_files/multi_tone_simulated_noise_1000hz_f0.wav")

        spectrum = SpectrumByFFT(wfm, fft_size=4096 * 2)
        f0 = swipe_spectral_estimation(spectrum, pitch_resolution=1 / 80)[0]

        self.assertAlmostEqual(1000, f0, delta=15)

        wfm = WaveFile(str(Path(__file__).parents[1]) +
                       "/Test Data/audio_files/multi_tone_change.wav")

        spectrum = SpectrumByFFT(wfm, fft_size=4096 * 2)
        f0 = swipe_spectral_estimation(spectrum, pitch_resolution=1 / 80)[0]

        self.assertAlmostEqual(300, f0, delta=15)

    def test_swipe(self):
        wfm = Waveform.generate_tone(1000)

        results = swipe(wfm.samples, wfm.sample_rate)

