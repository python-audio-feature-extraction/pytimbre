import datetime
import unittest
import pytest
import numpy as np
import doctest
from pytimbre.waveform import WeightingFunctions
from pytimbre.spectral.spectra import Spectrum, SpectrumByFFT, SpectrumByDigitalFilters, SpectrumEquivalentLevels #, SpectrumByCochleagram
from pytimbre.audio_files.wavefile import WaveFile, Waveform
from pathlib import Path
from pytimbre.spectral.time_histories import WeightingFunction, LeqDurationMode
from pytimbre.spectral.fractional_octave_band import FractionalOctaveBandTools as fob


class TestSpectrum(unittest.TestCase):
    def make_waveform(self):
        sample_rate = 1000
        max_frequency = 100
        self.amplitude_dB = 94
        amplitude_rms = 10 ** (self.amplitude_dB / 20) * 2e-5
        x = np.arange(0, 1, 1 / sample_rate)
        y = amplitude_rms * np.sqrt(2) * np.sin(2 * np.pi * max_frequency * x)

        return Waveform(y, sample_rate, 0)

    @staticmethod
    def multitone_noise_600hz():
        return str(Path(__file__).parents[1]) + "/Test Data/audio_files/multi_tone_simulated_noise_600hz_f0.wav"

    @staticmethod
    def multitone_noise_1000hz():
        return str(Path(__file__).parents[1]) + "/Test Data/audio_files/multi_tone_simulated_noise_1000hz_f0.wav"

    def test_spectrum_maker(self):
        f = np.arange(0, 1000)
        p = np.zeros(len(f))
        p[f == 125] = 10

        spec = Spectrum()
        spec.frequencies = f
        spec.pressures_pascals = p
        self.assertIsInstance(spec, Spectrum)
        self.assertIsNotNone(spec.frequencies)
        self.assertIsNotNone(spec.pressures_pascals)

        self.assertAlmostEqual(np.max(spec.pressures_decibels), 114, delta=1e-1)
        self.assertAlmostEqual(np.max(spec.pressures_pascals), 10, delta=1e-1)
        self.assertAlmostEqual(spec.overall_level, 114, delta=1e-1)
        self.assertAlmostEqual(spec.overall_a_weighted_level, 114 - 16.19, delta=1e-1)

    def test_calculate_engineering_unit_scale_factor(self):
        daq_system_gain_V_per_Pa = 1 / 5.0
        calibration_frequency = 250
        calibration_pressure_Pa_rms = 10.0
        calibration_level = 20 * np.log10(calibration_pressure_Pa_rms / 20e-6)

        signal_values_V_rms = calibration_pressure_Pa_rms * daq_system_gain_V_per_Pa
        f = np.array([125, 250, 500])
        p = np.zeros(len(f))
        p[f == calibration_frequency] = signal_values_V_rms

        spec = Spectrum()
        spec.frequencies = f
        spec.pressures_pascals = p
        spec.fractional_octave_bandwidth = 1

        sens_V_per_Pa = spec.calculate_engineering_unit_scale_factor(
            calibration_level=calibration_level,
            calibration_frequency=calibration_frequency
            )
        self.assertAlmostEqual(daq_system_gain_V_per_Pa, sens_V_per_Pa, delta=1e-8)

    def test_time(self):
        t0 = datetime.datetime.now()
        wfm = self.make_waveform()
        wfm.start_time = t0
        spec = Spectrum(wfm)

        self.assertEqual(t0, spec.time)

    def test_time_past_midnight(self):
        t0 = datetime.datetime.now()
        wfm = self.make_waveform()
        wfm.start_time = t0
        spec = Spectrum(wfm)

        tpm = 60 * (60 * t0.hour + t0.minute) + t0.second + t0.microsecond / 1e6
        self.assertEqual(tpm, spec.time_past_midnight)

    def test_SpectralEnergy(self):
        spec = Spectrum()
        spec.frequencies = np.arange(10, 100, 1)
        spec.pressures_pascals = 100 * np.ones(len(spec.frequencies))

        self.assertEqual(900000, spec.spectral_energy)

        spec = Spectrum()
        spec.frequencies = np.arange(10, 100, 1)
        spec.pressures_pascals = 100 * np.ones(len(spec.frequencies))
        spec.pressures_pascals = np.arange(0.1, 9.1, 0.1)

        self.assertEqual(2470.65, spec.spectral_energy)

    def test_SpectralFlatness(self):
        spec = Spectrum()
        spec.frequencies = np.arange(10, 100, 1)
        spec.pressures_pascals = 100 * np.ones(len(spec.frequencies))

        self.assertAlmostEqual(1, spec.spectral_flatness, delta=1e-6)

        spec = Spectrum()
        spec.frequencies = np.arange(10, 100, 1)
        spec.pressures_pascals = 100 * np.ones(len(spec.frequencies))
        spec.pressures_pascals = np.arange(0.1, 9.1, 0.1)

        self.assertAlmostEqual(0.753759, spec.spectral_flatness, delta=1e-5)

    def test_SpectralCrest(self):
        spec = Spectrum()
        spec.frequencies = np.arange(10, 100, 1)
        spec.pressures_pascals = 100 * np.ones(len(spec.frequencies))

        self.assertAlmostEqual(1, spec.spectral_crest, delta=1e-6)

        spec = Spectrum()
        spec.frequencies = np.arange(10, 100, 1)
        spec.pressures_pascals = 100 * np.ones(len(spec.frequencies))
        spec.pressures_pascals = np.arange(0.1, 9.1, 0.1)

        self.assertAlmostEqual(1.978021978, spec.spectral_crest, delta=1e-5)

    def test_SpectralRollOff(self):
        spec = Spectrum()
        spec.frequencies = np.arange(10, 100, 1)
        spec.pressures_pascals = 100 * np.ones(len(spec.frequencies))

        self.assertAlmostEqual(spec.frequencies[85], spec.spectral_roll_off, delta=1e-6)

        spec = Spectrum()
        spec.frequencies = np.arange(10, 100, 1)
        spec.pressures_pascals = 100 * np.ones(len(spec.frequencies))
        spec.pressures_pascals = np.arange(0.1, 9.1, 0.1)

        self.assertAlmostEqual(spec.frequencies[87], spec.spectral_roll_off, delta=1e-5)

    def test_SpectralCentroid(self):
        spec = Spectrum()
        spec.frequencies = np.arange(10, 100, 1)
        spec.pressures_pascals = 100 * np.ones(len(spec.frequencies))

        self.assertAlmostEqual(54.5, spec.spectral_centroid, delta=1e-6)

        spec = Spectrum()
        spec.frequencies = np.arange(10, 100, 1)
        spec.pressures_pascals = 100 * np.ones(len(spec.frequencies))
        spec.pressures_pascals = np.arange(0.1, 9.1, 0.1)

        self.assertAlmostEqual(69.3333333333333, spec.spectral_centroid, delta=1e-5)

    def test_SpectralSpread(self):
        spec = Spectrum()
        spec.frequencies = np.arange(10, 100, 1)
        spec.pressures_pascals = 100 * np.ones(len(spec.frequencies))

        self.assertAlmostEqual(25.97916, spec.spectral_spread, delta=1e-4)

        spec = Spectrum()
        spec.frequencies = np.arange(10, 100, 1)
        spec.pressures_pascals = 100 * np.ones(len(spec.frequencies))
        spec.pressures_pascals = np.arange(0.1, 9.1, 0.1)

        self.assertAlmostEqual(21.32812, spec.spectral_spread, delta=1e-4)

    def test_SpectralSkewness(self):
        spec = Spectrum()
        spec.frequencies = np.arange(10, 100, 1)
        spec.pressures_pascals = 100 * np.ones(len(spec.frequencies))

        self.assertAlmostEqual(0, spec.spectral_skewness, delta=1e-4)

        spec = Spectrum()
        spec.frequencies = np.arange(10, 100, 1)
        spec.pressures_pascals = 100 * np.ones(len(spec.frequencies))
        spec.pressures_pascals = np.arange(0.1, 9.1, 0.1)

        self.assertAlmostEqual(-0.56576, spec.spectral_skewness, delta=1e-4)

    def test_SpectralKutosis(self):
        spec = Spectrum()
        spec.frequencies = np.arange(10, 100, 1)
        spec.pressures_pascals = 100 * np.ones(len(spec.frequencies))

        self.assertAlmostEqual(1.799704, spec.spectral_kurtosis, delta=1e-4)

        spec = Spectrum()
        spec.frequencies = np.arange(10, 100, 1)
        spec.pressures_pascals = 100 * np.ones(len(spec.frequencies))
        spec.pressures_pascals = np.arange(0.1, 9.1, 0.1)

        self.assertAlmostEqual(2.39956, spec.spectral_kurtosis, delta=1e-4)

    def test_SpectralSlope(self):
        spec = Spectrum()
        spec.frequencies = np.arange(10, 100, 1)
        spec.pressures_pascals = 100 * np.ones(len(spec.frequencies))

        self.assertAlmostEqual(0.0, spec.spectral_slope, delta=1e-4)

        spec = Spectrum()
        spec.frequencies = np.arange(10, 100, 1)
        spec.pressures_pascals = 100 * np.ones(len(spec.frequencies))
        spec.pressures_pascals = np.arange(0.1, 9.1, 0.1)

        self.assertAlmostEqual(0.00024420024420024436, spec.spectral_slope, delta=1e-4)

    def test_SpectralDecrease(self):
        spec = Spectrum()
        spec.frequencies = np.arange(10, 100, 1)
        spec.pressures_pascals = 100 * np.ones(len(spec.frequencies))

        self.assertAlmostEqual(0.0, spec.spectral_decrease, delta=1e-4)

        spec = Spectrum()
        spec.frequencies = np.arange(10, 100, 1)
        spec.pressures_pascals = 100 * np.ones(len(spec.frequencies))
        spec.pressures_pascals = np.arange(0.1, 9.1, 0.1)

        self.assertAlmostEqual(0.0217391304347826, spec.spectral_decrease, delta=1e-4)

    def test_partial_indices(self):
        spl = np.array(
            [43.81180328, 46.89868852, 46.42377049, 47.89754098, 49.62606557, 50.17213115,
             46.9657377, 46.99032787, 51.1152459, 51.49819672, 47.94819672, 49.18295082, 46.24163934, 44.61327869,
             41.70885246, 40.96098361, 35.06508197, 46.75557377, 52.37557377, 41.23377049, 40.68721311, 45.25786885,
             42.21229508, 41.19737705, 42.23836066, 40.64360656, 39.1752459, 40.44131148, 44.30557377, 45.8204918,
             44.17]
            )
        spec = Spectrum()
        spec.frequencies = fob.tob_frequencies()
        spec.pressures_decibels = spl

        self.assertAlmostEqual(629.96, spec.fundamental_frequency, delta=9)
        spec._calculate_partial_pressures()
        pi = spec.partial_frequencies_indices
        self.assertEqual(5, len(pi))
        indices = [18, 21, 24, 27, 30]

        for i in range(len(pi)):
            self.assertEqual(indices[i], pi[i])

    def test_harmonic_features(self):
        spl = np.array([43.81180328,    46.89868852, 46.42377049, 47.89754098, 49.62606557, 50.17213115,
                46.9657377,  46.99032787, 51.1152459,  51.49819672, 47.94819672, 49.18295082, 46.24163934, 44.61327869,
                41.70885246, 40.96098361, 35.06508197, 46.75557377, 52.37557377, 41.23377049, 40.68721311, 45.25786885,
                42.21229508, 41.19737705, 42.23836066, 40.64360656, 39.1752459,  40.44131148, 44.30557377, 45.8204918,
                        44.17 ])
        spec = Spectrum()
        spec.frequencies = fob.tob_frequencies()
        spec.pressures_decibels = spl

        self.assertAlmostEqual(629.96, spec.fundamental_frequency, delta=9)
        self.assertAlmostEqual(0.0001041188073145650, spec.harmonic_energy, delta=1e-8)
        self.assertAlmostEqual(0.0004713855211525160, spec.noise_energy, delta=1e-8)
        self.assertAlmostEqual(0.0001167237245641360, spec.harmonic_spectral_deviation, delta=1e-8)
        self.assertAlmostEqual(22.5770798123561000000, spec.inharmonicity, delta=2e-7)
        self.assertAlmostEqual(0.8190824948408350000, spec.noisiness, delta=1e-8)
        self.assertAlmostEqual(0.9605137140466730000, spec.odd_even_ratio, delta=1e-8)
        self.assertAlmostEqual(0.4177396858692690000, spec.tri_stimulus[0], delta=1e-8)
        self.assertAlmostEqual(0.4198449734904560000, spec.tri_stimulus[1], delta=1e-8)
        self.assertAlmostEqual( 0.1624153406402750000, spec.tri_stimulus[2], delta=1e-8)

    def test_equivalent_levels(self):
        spec = Spectrum()
        spec.frequencies = np.array([500, 1000, 2000])
        spec.pressures_decibels = np.array([-np.inf, 114.0, -np.inf])

        leq = spec.equivalent_levels(equivalent_duration=datetime.timedelta(hours=4))

        test_decibels = [-np.inf, 114., -np.inf]
        test_pascals = [0., 10., 0.]
        for i in range(len(spec.frequencies)):
            self.assertEqual(leq.equivalent_pressure_decibels[i], test_decibels[i])
            self.assertAlmostEqual(leq.equivalent_pressure_pascals[i], test_pascals[i], delta=0.1)

        self.assertEqual(leq.equivalent_duration.total_seconds(), 4 * 60 * 60)
        self.assertEqual(leq.weighting, WeightingFunctions.unweighted)
        self.assertAlmostEqual(leq.sel, 155.58, delta=0.1)
        self.assertAlmostEqual(leq.leq8hr, 111.0, delta=0.1)
        self.assertAlmostEqual(leq.noise_dose_pct, 100 * 2 ** ((111 - 85) / 3), delta=leq.noise_dose_pct / 100)

    def test_roughness(self):
        from mosqito.sq_metrics import roughness_dw_freq

        spl = np.array([43.81180328,    46.89868852, 46.42377049, 47.89754098, 49.62606557, 50.17213115,
                46.9657377,  46.99032787, 51.1152459,  51.49819672, 47.94819672, 49.18295082, 46.24163934, 44.61327869,
                41.70885246, 40.96098361, 35.06508197, 46.75557377, 52.37557377, 41.23377049, 40.68721311, 45.25786885,
                42.21229508, 41.19737705, 42.23836066, 40.64360656, 39.1752459,  40.44131148, 44.30557377, 45.8204918,
                        44.17])
        spec = Spectrum()
        spec.frequencies = fob.tob_frequencies()
        spec.pressures_decibels = spl

        hw = roughness_dw_freq(spl, fob.tob_frequencies())

    def test_sharpness(self):
        from mosqito.sq_metrics import sharpness_din_freq

        spl = np.array([43.81180328,    46.89868852, 46.42377049, 47.89754098, 49.62606557, 50.17213115,
                46.9657377,  46.99032787, 51.1152459,  51.49819672, 47.94819672, 49.18295082, 46.24163934, 44.61327869,
                41.70885246, 40.96098361, 35.06508197, 46.75557377, 52.37557377, 41.23377049, 40.68721311, 45.25786885,
                42.21229508, 41.19737705, 42.23836066, 40.64360656, 39.1752459,  40.44131148, 44.30557377, 45.8204918,
                        44.17])
        spec = Spectrum()
        spec.frequencies = fob.tob_frequencies()
        spec.pressures_decibels = spl

        hw = sharpness_din_freq(spl, fob.tob_frequencies())

    def test_loudness(self):
        from mosqito.sq_metrics import loudness_zwst_freq

        spl = np.array([43.81180328,    46.89868852, 46.42377049, 47.89754098, 49.62606557, 50.17213115,
                46.9657377,  46.99032787, 51.1152459,  51.49819672, 47.94819672, 49.18295082, 46.24163934, 44.61327869,
                41.70885246, 40.96098361, 35.06508197, 46.75557377, 52.37557377, 41.23377049, 40.68721311, 45.25786885,
                42.21229508, 41.19737705, 42.23836066, 40.64360656, 39.1752459,  40.44131148, 44.30557377, 45.8204918,
                        44.17, 0])
        spec = Spectrum()
        spec.frequencies = fob.tob_frequencies()
        spec.pressures_pascals = 20e-6 * 10**(spl/20)

        hw = loudness_zwst_freq(20e-6 * 10**(spl[4:]/20),
                                1000 * 2**((np.arange(14,42,1)-30)/3));


class Test_SpectrumByFFT(unittest.TestCase):
    @staticmethod
    def pink():
        return str(
            Path(__file__).parents[1]
        ) + '/Test Data/audio_files/Files/canonical wave file/pink_44100Hz_32bit.wav'

    @staticmethod
    def pink_noise_waveform():
        wav = WaveFile(Test_SpectrumByFFT.pink())

        return Waveform(
            pressures=wav.samples,
            sample_rate=wav.sample_rate,
            start_time=wav.start_time
            )

    def test_calculate_spectrum(self):
        freq = 125
        amp_db = 114
        amplitude_rms = 10 ** (amp_db / 20) * 20e-6
        waveform = Waveform.generate_tone(frequency=freq, amplitude_db=amp_db)

        spectrum = SpectrumByFFT(waveform, fft_size=len(waveform.samples))
        spectrum._calculate_spectrum()

        # test _acoustic_pressures_pascals via amplitude of tone
        self.assertAlmostEqual(spectrum.overall_level, amp_db, delta=1e-5)

        # test _frequencies via identification of tone frequency
        max_f = spectrum._frequencies_nb[abs(spectrum._frequencies_nb - freq) ==
                                         np.min(np.abs(spectrum._frequencies_nb - freq))]
        self.assertAlmostEqual(max_f, freq, delta=1e-5)
        max_f = spectrum._frequencies[abs(spectrum._frequencies - freq) == np.min(np.abs(spectrum._frequencies - freq))]
        self.assertAlmostEqual(max_f, freq, delta=1e-5)

    def test_impulse_calculate_spectrum(self):
        from pytimbre.waveform import AnalysisMethod

        # Test that leqt = OASPL_TOB = OASPL_NB within 0.3 dB for a range of Friedlander waves with various peak/a_durs.
        a_durations = np.array([1e-4, 2.5e-4, 5e-4, 1e-3, 2.5e-3, 5e-3, 1e-2])
        peak_levels = np.arange(130, 175, 5)  # dB
        dur = 1  # s
        sample_rate = 200e3  # hz
        bt = 0.5  # s

        for peak in peak_levels:
            for a_dur in a_durations:
                wfm = Waveform.generate_friedlander(
                    peak_level=peak,
                    a_duration=a_dur,
                    duration=dur,
                    sample_rate=sample_rate,
                    blast_time=bt,
                    noise=False
                    )
                wfm.is_impulsive = True

                spec = SpectrumByFFT(wfm)
                spec2 = spec.to_fractional_octave_band(bandwidth=3, f0=1, f1=100000)

                iwfm = Waveform(wfm.samples, wfm.sample_rate, wfm.start_time, is_continuous_wfm=False)
                iwfm.impulse_analysis_method = AnalysisMethod.NO_A_DURATION_CORRECTIONS

                # Check that FOB overall level and NB overall level are within .5 dB of each other
                self.assertAlmostEqual(spec.overall_level, spec2.overall_level, delta=0.3)

                # Check that NB overall level and leqT are within .5 dB of each other
                self.assertAlmostEqual(spec.overall_level, iwfm.leqT, delta=0.3)

                # Check that FOB overall level and NB overall level are within .5 dB of each other
                self.assertAlmostEqual(spec2.overall_level, iwfm.leqT, delta=0.3)

        # TODO - Need more rigourous testing of this class!!!

    def test_frequencies(self):
        waveform = Waveform.generate_tone()

        spectrum = SpectrumByFFT(waveform, fft_size=100)
        df = spectrum.frequencies[1] - spectrum.frequencies[0]

        self.assertEqual(len(spectrum.frequencies), 50)
        self.assertEqual(spectrum.frequencies[-1], (spectrum.sample_rate / 2) - df)

    def test_frequencies_double_sided(self):
        waveform = Waveform.generate_tone()

        spectrum = SpectrumByFFT(waveform, fft_size=100)
        df = spectrum.frequencies[1] - spectrum.frequencies[0]

        self.assertEqual(len(spectrum.frequencies_double_sided), 100)
        self.assertEqual(spectrum.frequencies_double_sided[-1], spectrum.sample_rate - df)

        self.assertEqual(len(spectrum._frequencies_double_sided), 100)
        self.assertEqual(spectrum._frequencies_double_sided[-1], spectrum.sample_rate - df)

    def test_fft_size(self):
        waveform = Waveform.generate_tone()

        spectrum = SpectrumByFFT(waveform, fft_size=100)
        self.assertEqual(spectrum.fft_size, 100)

        spectrum = SpectrumByFFT(waveform)
        self.assertEqual(spectrum.fft_size, 2 ** 15)

        self.assertRaises(ValueError, SpectrumByFFT, waveform, fft_size=len(waveform.samples) + 1)

    def test_if_pressures_array_has_correct_number_of_blocks(self):
        waveform = Waveform.generate_tone()

        spectrum = SpectrumByFFT(waveform, fft_size=100)
        self.assertListEqual(list(np.shape(spectrum.pressures_complex_double_sided)), [959, 100])

        self.assertEqual(1, len(spectrum.pressures_pascals.shape))
        self.assertEqual(50, len(spectrum.pressures_pascals))

    def test_frequency_increment(self):
        waveform = Waveform.generate_tone()

        spectrum = SpectrumByFFT(waveform, fft_size=100)

        self.assertEqual(waveform.sample_rate / spectrum.fft_size, spectrum.frequency_increment)

    def test_power_spectrum_max_level_tone(self):
        max_frequency = 1250
        max_level = 114
        waveform = Waveform.generate_tone(frequency=max_frequency, amplitude_db=max_level)
        spectrum = SpectrumByFFT(waveform)

        test_max_level = np.max(spectrum.pressures_decibels)
        self.assertAlmostEqual(max_level, test_max_level, delta=3)

        test_max_frequency = spectrum.frequencies[spectrum.pressures_decibels == test_max_level]
        self.assertAlmostEqual(max_frequency, test_max_frequency, delta=1)

    def test_overall_level(self):
        waveform = Waveform.generate_tone(amplitude_db=114)

        spectrum = SpectrumByFFT(waveform)
        self.assertAlmostEqual(spectrum.overall_level, waveform.overall_level(), delta=0.2)
        self.assertAlmostEqual(
            waveform.overall_level(weighting=WeightingFunctions.a_weighted),
            spectrum.overall_a_weighted_level,
            delta=0.5
            )

        waveform = Test_SpectrumByFFT.pink_noise_waveform()

        spectrum = SpectrumByFFT(waveform)
        self.assertAlmostEqual(spectrum.overall_level, waveform.overall_level(), delta=0.2)
        self.assertAlmostEqual(
            waveform.overall_level(weighting=WeightingFunctions.a_weighted),
            spectrum.overall_a_weighted_level,
            delta=0.5
            )

    def test_power_spectral_density_overall_level(self):
        waveform = Test_SpectrumByFFT.pink_noise_waveform()

        spec = SpectrumByFFT(waveform)
        df = spec.frequencies[1] - spec.frequencies[0]

        psd_pa = spec.power_spectral_density
        lf = 10 * np.log10(df * np.sum(psd_pa ** 2) / 20e-6 / 20e-6)
        self.assertAlmostEqual(waveform.overall_level(), lf, delta=3e-1)

        waveform = Waveform.generate_tone(amplitude_db=114)

        spec = SpectrumByFFT(waveform)
        df = spec.frequencies[1] - spec.frequencies[0]

        psd_pa = spec.power_spectral_density
        lf = 10 * np.log10(df * np.sum(psd_pa ** 2) / 20e-6 / 20e-6)
        self.assertAlmostEqual(waveform.overall_level(), lf, delta=3e-1)

    def test_spectrum_maker(self):
        amp_pa = 10
        f_step = 2
        f = np.arange(0, 1000, step=f_step)
        p = np.zeros(len(f))
        p[f == 126] = amp_pa

        spec = SpectrumByFFT()
        spec.frequencies = f
        spec.pressures_pascals = p
        self.assertIsInstance(spec, Spectrum)
        self.assertIsNotNone(spec.frequencies)
        self.assertIsNotNone(spec.pressures_pascals)

        self.assertAlmostEqual(np.max(spec.pressures_decibels), 114, delta=1e-1)
        self.assertAlmostEqual(spec.overall_level, 114, delta=1e-1)
        self.assertEqual(spec.frequency_increment, f_step)
        self.assertAlmostEqual(np.max(spec.power_spectral_density), amp_pa / np.sqrt(2), delta=0.01)

    def test_pink_noise_for_flatness(self):

        # generate pink noise spectrum
        step_f = 0.05
        f_nb = np.arange(0, 1000, step=step_f)
        p = 1 / np.sqrt(f_nb)
        p[0] = 1000

        # verify that power spectral density rolls off with 10 dB per decade of frequency
        psd = 10 * np.log10((1 / step_f) * p ** 2)
        for f_check in [1, 2, 10, 20, 100, 200]:
            self.assertAlmostEqual(psd[f_nb == f_check], psd[f_nb == f_check / 10] - 10, delta=1e-6)

        s = Spectrum()
        s.frequencies = f_nb
        s.pressures_pascals = p

        f_fob, p_fob = SpectrumByFFT.convert_nb_to_fob(
            s.frequencies, s.pressures_pascals, fob_band_width=1, f0=16,
            f1=500
            )
        for i in range(1, len(f_fob)):
            self.assertAlmostEqual(p_fob[i], p_fob[i - 1], delta=0.01)

        s_fob = SpectrumByFFT.convert_nb_to_fob(s.frequencies, s.pressures_pascals, fob_band_width=3, f0=16, f1=500)
        for i in range(1, len(f_fob)):
            self.assertAlmostEqual(p_fob[i], p_fob[i - 1], delta=0.01)

        s_fob = SpectrumByFFT.convert_nb_to_fob(s.frequencies, s.pressures_pascals, fob_band_width=6, f0=16, f1=500)
        for i in range(1, len(f_fob)):
            self.assertAlmostEqual(p_fob[i], p_fob[i - 1], delta=0.01)

        s_fob = SpectrumByFFT.convert_nb_to_fob(s.frequencies, s.pressures_pascals, fob_band_width=12, f0=16, f1=500)
        for i in range(1, len(f_fob)):
            self.assertAlmostEqual(p_fob[i], p_fob[i - 1], delta=0.01)

        s_fob = SpectrumByFFT.convert_nb_to_fob(s.frequencies, s.pressures_pascals, fob_band_width=24, f0=16, f1=500)
        for i in range(1, len(f_fob)):
            self.assertAlmostEqual(p_fob[i], p_fob[i - 1], delta=0.01)

    def test_overall_level_with_tone(self):

        # generate tonal spectrum
        amp_db = 114
        f_nb = np.arange(0, 1000, step=0.1)
        p = np.zeros(len(f_nb))
        p[f_nb == 125] = 10 ** (amp_db / 20) * 2e-5

        f, fob = SpectrumByFFT.convert_nb_to_fob(f_nb, p, fob_band_width=1, f0=16, f1=500)
        lf = 10 * np.log10(sum(fob ** 2) / 20e-6 / 20e-6)
        self.assertAlmostEqual(lf, amp_db, delta=0.1)

        f, fob = SpectrumByFFT.convert_nb_to_fob(f_nb, p, fob_band_width=3, f0=16, f1=500)
        lf = 10 * np.log10(sum(fob ** 2) / 20e-6 / 20e-6)
        self.assertAlmostEqual(lf, amp_db, delta=0.1)

        f, fob = SpectrumByFFT.convert_nb_to_fob(f_nb, p, fob_band_width=6, f0=16, f1=500)
        lf = 10 * np.log10(sum(fob ** 2) / 20e-6 / 20e-6)
        self.assertAlmostEqual(lf, amp_db, delta=0.2)

        f, fob = SpectrumByFFT.convert_nb_to_fob(f_nb, p, fob_band_width=12, f0=16, f1=500)
        lf = 10 * np.log10(sum(fob ** 2) / 20e-6 / 20e-6)
        self.assertAlmostEqual(lf, amp_db, delta=0.2)

        f, fob = SpectrumByFFT.convert_nb_to_fob(f_nb, p, fob_band_width=24, f0=16, f1=500)
        lf = 10 * np.log10(sum(fob ** 2) / 20e-6 / 20e-6)
        self.assertAlmostEqual(lf, amp_db, delta=0.2)

    def test_frequency_bin_with_tone(self):

        # generate tonal spectrum
        f_nb = np.arange(0, 1000, step=0.1)
        p = np.zeros(len(f_nb))
        p[f_nb == 125] = 1

        f, fob = SpectrumByFFT.convert_nb_to_fob(f_nb, p, fob_band_width=1, f0=16, f1=500)
        self.assertEqual(fob[np.abs(f - 125) == np.min(np.abs(f - 125))], np.max(fob))

        f, fob = SpectrumByFFT.convert_nb_to_fob(f_nb, p, fob_band_width=3, f0=16, f1=500)
        self.assertEqual(fob[np.abs(f - 125) == np.min(np.abs(f - 125))], np.max(fob))

        # generate tonal spectrum
        f_nb = np.arange(0, 1000, step=0.1)
        p = np.zeros(len(f_nb))
        p[f_nb == 153] = 1

        f, fob = SpectrumByFFT.convert_nb_to_fob(f_nb, p, fob_band_width=6, f0=16, f1=500)
        self.assertEqual(fob[np.abs(f - 153) == np.min(np.abs(f - 153))], np.max(fob))

        f, fob = SpectrumByFFT.convert_nb_to_fob(f_nb, p, fob_band_width=12, f0=16, f1=500)
        self.assertEqual(fob[np.abs(f - 153) == np.min(np.abs(f - 153))], np.max(fob))

        f, fob = SpectrumByFFT.convert_nb_to_fob(f_nb, p, fob_band_width=24, f0=16, f1=500)
        self.assertEqual(fob[np.abs(f - 153) == np.min(np.abs(f - 153))], np.max(fob))

    def test_calculate_engineering_unit_scale_factor(self):
        daq_system_gain_V_per_Pa = 1 / 5.0
        calibration_frequency = 250
        calibration_pressure_Pa_rms = 10.0
        calibration_level = 20 * np.log10(calibration_pressure_Pa_rms / 20e-6)
        signal_values_V_rms = calibration_pressure_Pa_rms * daq_system_gain_V_per_Pa
        signal_level = 20 * np.log10(signal_values_V_rms / 20e-6)
        signal = Waveform.generate_tone(frequency=calibration_frequency, amplitude_db=signal_level)
        spec = SpectrumByFFT(signal)

        with pytest.raises(ValueError):
            spec.calculate_engineering_unit_scale_factor(
                calibration_level=calibration_level,
                calibration_frequency=calibration_frequency
                )

        spec_ob = spec.to_fractional_octave_band(bandwidth=1)
        sens_V_per_Pa = spec_ob.calculate_engineering_unit_scale_factor(
            calibration_level=calibration_level,
            calibration_frequency=calibration_frequency
            )
        self.assertAlmostEqual(daq_system_gain_V_per_Pa, sens_V_per_Pa, delta=1e-8)

    def test_fundamental_frequency(self):
        wfm = Waveform.generate_tone(1000)
        spec = SpectrumByFFT(wfm)

        f0 = spec.fundamental_frequency

        self.assertAlmostEqual(1000, f0, delta=16)

    def test_harmonic_energy(self):
        wfm = Waveform.generate_tone(1000)

        spectrum = SpectrumByFFT(wfm, fft_size=4096)

        self.assertAlmostEqual(1000, spectrum.fundamental_frequency, delta=20)

        self.assertAlmostEqual(0.0058, spectrum.harmonic_energy, delta=1e-2)

    def test_noise_energy(self):
        wfm = Waveform.generate_tone(1000)

        spectrum = SpectrumByFFT(wfm, fft_size=4096)

        self.assertAlmostEqual(0.998, spectrum.noise_energy, delta=1e-2)

    def test_noisiness(self):
        wfm = Waveform.generate_tone(1000)

        spectrum = SpectrumByFFT(wfm, fft_size=4096)

        self.assertAlmostEqual(0.998 / 1.003, spectrum.noisiness, delta=1e-2)

    def test_tristim(self):
        wfm = Waveform.generate_tone(1000)

        spectrum = SpectrumByFFT(wfm, fft_size=4096)
        tri_stimulus = spectrum.tri_stimulus

        self.assertAlmostEqual(0.99, tri_stimulus[0], delta=1e-2)
        self.assertAlmostEqual(1e-7, tri_stimulus[1], delta=1e-2)
        self.assertAlmostEqual(1e-8, tri_stimulus[2], delta=1e-2)

    def test_harmonic_deviation(self):
        wfm = Waveform.generate_tone(1000)

        spectrum = SpectrumByFFT(wfm, fft_size=4096)

        self.assertAlmostEqual(-0.0011, spectrum.harmonic_spectral_deviation, delta=1e-3)

    def test_odd_even_ratio(self):
        wfm = Waveform.generate_tone(1000)

        spectrum = SpectrumByFFT(wfm, fft_size=4096)

        self.assertAlmostEqual(0.0179, spectrum.odd_even_ratio, delta=1e-3)

    def test_inharmonicity(self):
        wfm = Waveform.generate_tone(1000)

        spectrum = SpectrumByFFT(wfm, fft_size=4096)

        self.assertAlmostEqual(0.001, spectrum.inharmonicity, delta=1e-3)

    def test_get_average_features(self):
        wfm = Waveform.generate_tone(1000, amplitude_db=91)

        spectrum = SpectrumByFFT(wfm, fft_size=4096)

        features = spectrum.get_average_features()
        # self.assertAlmostEqual(0.5, features['spectral_energy'])

        self.assertEqual(86, len(features))

        features = spectrum.get_average_features(include_sq_metrics=False)
        self.assertEqual(82, len(features))


class Test_SpectrumByDigitalFilters(unittest.TestCase):
    @staticmethod
    def pink_noise_file():
        from pathlib import Path

        return str(
            Path(__file__).parents[1]
        ) + '/Test Data/audio_files/Files/canonical wave file/pink_44100Hz_32bit.wav'

    def test_constructor(self):
        bank = SpectrumByDigitalFilters(WaveFile(Test_SpectrumByDigitalFilters.pink_noise_file()))

    def test_settle_time(self):
        bank = SpectrumByDigitalFilters(WaveFile(Test_SpectrumByDigitalFilters.pink_noise_file()))

        self.assertTrue(bank.settle_samples < 25000)
        self.assertTrue(bank.settle_time > 0.25)

    def test_calculate_spectrum(self):
        wfm = WaveFile(Test_SpectrumByDigitalFilters.pink_noise_file(), s0=0, s1=44100)

        bank = SpectrumByDigitalFilters(wfm)

        self.assertEqual(31, len(bank.frequencies))
        self.assertAlmostEqual(np.round(bank.waveform.overall_level()), np.round(bank.overall_level), delta=1.0)

        wfm = WaveFile(Test_SpectrumByDigitalFilters.pink_noise_file(), s0=0, s1=(int(np.floor(44100 / 4))))
        bank = SpectrumByDigitalFilters(wfm)

        self.assertEqual(30, len(bank.frequencies))
        self.assertAlmostEqual(12.5, bank.frequencies[0], delta=1)
        self.assertAlmostEqual(np.round(bank.waveform.overall_level()), np.round(bank.overall_level), delta=3.0)

        bank = SpectrumByDigitalFilters(Waveform.generate_tone(duration=1))

        self.assertEqual(31, len(bank.frequencies))
        self.assertAlmostEqual(np.round(bank.waveform.overall_level()), np.round(bank.overall_level), delta=1.0)


class TestSpectrumEquivalentLevels(unittest.TestCase):
    def test_constructor(self):
        spec = Spectrum()
        spec.frequencies = np.array([500, 1000, 2000])
        spec.pressures_decibels = np.array([-np.inf, 114.0, -np.inf])
        leq = SpectrumEquivalentLevels(
            spectrum=spec,
            equivalent_duration=datetime.timedelta(hours=4),
            weighting=WeightingFunctions.unweighted
        )
        self.assertIsInstance(leq.equivalent_pressure_decibels, np.ndarray)
        self.assertIsInstance(leq.equivalent_pressure_pascals, np.ndarray)
        self.assertEqual(leq.equivalent_duration.total_seconds(), 4 * 60 * 60)
        self.assertEqual(leq.weighting, WeightingFunctions.unweighted)

    def test_a_weighting(self):
        spec = Spectrum()
        spec.frequencies = [500, 1000, 2000]
        spec.pressures_decibels = [114.0 + 3.2, 114.0, -np.inf]
        leq = SpectrumEquivalentLevels(
            spectrum=spec,
            equivalent_duration=datetime.timedelta(hours=4),
            weighting=WeightingFunctions.a_weighted
        )
        test_decibels = [114., 114., -np.inf]
        test_pascals = [10., 10., 0.]
        for i in range(len(spec.frequencies)):
            self.assertAlmostEqual(leq.equivalent_pressure_decibels[i], test_decibels[i], delta=0.1)
            self.assertAlmostEqual(leq.equivalent_pressure_pascals[i], test_pascals[i], delta=0.1)
        self.assertAlmostEqual(leq.leq8hr, 114.0 + 3.0 - 3.0, delta=0.1)
        self.assertAlmostEqual(leq.sel, 158.58, delta=0.1)

    def test_c_weighting(self):
        spec = Spectrum()
        spec.frequencies = np.array([20, 1000, 2000])
        spec.pressures_decibels = np.array([114.0 + 6.2, 114.0, -np.inf])
        leq = SpectrumEquivalentLevels(
            spectrum=spec,
            equivalent_duration=datetime.timedelta(hours=4),
            weighting=WeightingFunctions.c_weighted
        )
        test_decibels = [114., 114., -np.inf]
        test_pascals = [10., 10., 0.]
        for i in range(len(spec.frequencies)):
            self.assertAlmostEqual(leq.equivalent_pressure_decibels[i], test_decibels[i], delta=0.1)
            self.assertAlmostEqual(leq.equivalent_pressure_pascals[i], test_pascals[i], delta=0.1)
        self.assertAlmostEqual(leq.leq8hr, 114.0 + 3.0 - 3.0, delta=0.1)
        self.assertAlmostEqual(leq.sel, 158.58, delta=0.1)

    def test_if_noise_dose_is_invariant_with_input_weighting(self):
        spec = Spectrum()
        spec.frequencies = [500, 1000, 2000]
        spec.pressures_decibels = [85. + 3.2, 85, -np.inf]

        leq_a = SpectrumEquivalentLevels(
            spectrum=spec,
            equivalent_duration=datetime.timedelta(hours=8),
            weighting=WeightingFunctions.a_weighted
        )
        self.assertAlmostEqual(leq_a.leq8hr, 85. + 3.0, delta=0.1)
        self.assertAlmostEqual(leq_a.noise_dose_pct, 200., delta=1)

        leq_c = SpectrumEquivalentLevels(
            spectrum=spec,
            equivalent_duration=datetime.timedelta(hours=8),
            weighting=WeightingFunctions.c_weighted
        )
        self.assertAlmostEqual(leq_c.noise_dose_pct, 200., delta=1)

        leq_z = SpectrumEquivalentLevels(
            spectrum=spec,
            equivalent_duration=datetime.timedelta(hours=8),
            weighting=WeightingFunctions.unweighted
        )
        self.assertAlmostEqual(leq_z.noise_dose_pct, 200., delta=1)

    def test_leq_convert_duration(self):
        pressures_decibels = np.array([114.0, 85.0, 17.0])
        test_leq = SpectrumEquivalentLevels.leq_convert_duration(pressures_decibels, tin=1.0, tout=2.0)
        self.assertIsInstance(test_leq, np.ndarray)
        for i in range(len(pressures_decibels)):
            self.assertAlmostEqual(test_leq[i], pressures_decibels[i] - 3.0, delta=0.1)

