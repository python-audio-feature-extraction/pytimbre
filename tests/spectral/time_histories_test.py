import datetime
import unittest
import numpy as np
import pandas as pd
import pathlib
import os
from pytimbre.waveform import Waveform, WeightingFunctions, FrameBuilder
from pytimbre.audio_files.wavefile import WaveFile
from pytimbre.audio_files.ansi_standard_formatted_files import StandardBinaryFile
from pytimbre.spectral.time_histories import NarrowbandTimeHistory, LogarithmicBandTimeHistory, TimeHistory, \
    LeqDurationMode, Spectrogram
from pytimbre.spectral.time_histories import WeightingFunction as th_weighting_function


class test_Timehistory(unittest.TestCase):
    @staticmethod
    def time_history_example():
        from pathlib import Path

        return str(Path(__file__).parents[1]) + "/Test Data/Spectral/SITE01_RUN104.tob.csv"

    @staticmethod
    def time_history_example_steady_state():
        from pathlib import Path
        return str(Path(__file__).parents[1]) + \
               "/Test Data/Spectral/Std_TOB_Testfile_steady_state.spl.csv"

    @staticmethod
    def time_history_example_transient():
        from pathlib import Path
        return str(Path(__file__).parents[1]) + \
               "/Test Data/Spectral/Std_TOB_Testfile_transient.spl.csv"

    @staticmethod
    def multitone_noise_change():
        from pathlib import Path
        return str(Path(__file__).parents[1]) + "/Test Data/audio_files/multi_tone_change.wav"

    @staticmethod
    def std_bin_example():
        from pathlib import Path

        return str(Path(__file__).parents[1]) + "/Test " \
                                                "Data/audio_files/Files/standard_bin_file_write/" \
                                                "test_standard_bin_file.bin"

    def test_duration(self):
        th = TimeHistory.load(test_Timehistory.time_history_example())
        self.assertEqual(47 * 0.25, th.duration)

    def test_load(self):
        th = TimeHistory.load(test_Timehistory.time_history_example())
        self.assertEqual(1 / 0.25, th.time_history_sample_rate)
        self.assertEqual(29, len(th._header))
        self.assertEqual(47, len(th.spectra))
        for i in range(47):
            self.assertIsNotNone(th.spectra[i])
        self.assertIsNone(th.waveform)
        self.assertEqual(th.header['TEST LOCATION'], 'Nowhere Special')
        self.assertAlmostEqual(59381.851, th.times[0], delta=0.001)

    def test_if_load_retains_correct_spl_magnitudes(self):
        th = TimeHistory.load(test_Timehistory.time_history_example_steady_state())
        for i in range(len(th.times)):
            self.assertAlmostEqual(84.5, th.overall_level[i], delta=0.501)
            self.assertEqual(
                datetime.datetime(year=2020, month=1, day=22, hour=12, minute=0, second=0) +
                datetime.timedelta(microseconds=i * 500000),
                th.spectra[i].time
            )
            self.assertAlmostEqual(0.33, th.spectra[i].pressures_pascals[7], delta=0.03)
            self.assertAlmostEqual(84.5, th.spectra[i].pressures_decibels[7], delta=0.501)

    def test_if_save_replaces_unwanted_strings_in_header(self):
        th = TimeHistory.load(test_Timehistory.time_history_example())
        th._header = {'X,M' if k == 'X' else k: v for k, v in th._header.items()}
        th.save('temp_timehist_fortest.spl.csv')

        th_test = TimeHistory.load('temp_timehist_fortest.spl.csv')
        self.assertIsNotNone(th_test._header['X_M'])
        os.remove('temp_timehist_fortest.spl.csv')

    def test_sample_rates(self):
        wfm = StandardBinaryFile(test_Timehistory.std_bin_example())
        integration_time = 1.0
        th = NarrowbandTimeHistory(wfm, integration_time=integration_time)
        self.assertEqual(wfm.sample_rate, th.waveform.sample_rate)
        self.assertEqual(wfm.sample_rate, th.spectra[0].waveform.sample_rate)
        self.assertEqual(wfm.sample_rate, th.waveform_sample_rate)
        self.assertEqual(1 / integration_time, th.time_history_sample_rate)

    def test_sample_size(self):
        wfm = StandardBinaryFile(test_Timehistory.std_bin_example())
        waveform_sample_rate = wfm.sample_rate
        integration_time = 1.0
        th = NarrowbandTimeHistory(wfm, integration_time=integration_time)
        self.assertEqual(int(integration_time * waveform_sample_rate), th.waveform_sample_size)

    def test_if_equivalent_level_returns_correct_magnitudes(self):
        th_steady_state = TimeHistory.load(test_Timehistory.time_history_example_steady_state())
        th_transient = TimeHistory.load(test_Timehistory.time_history_example_transient())

        leq_transient = th_transient.equivalent_level(
            weighting=th_weighting_function.unweighted,
            leq_mode=LeqDurationMode.transient,
            equivalent_duration=th_transient.duration,
        )
        self.assertAlmostEqual(84.5 - 3, leq_transient, delta=0.501)

        leq_transient2 = th_transient.equivalent_level(
            weighting=th_weighting_function.unweighted,
            leq_mode=LeqDurationMode.transient,
            equivalent_duration=th_transient.duration * 2,
        )
        self.assertAlmostEqual(84.5 - 3 - 3, leq_transient2, delta=0.501)

        leq_steady_state = th_steady_state.equivalent_level(
            weighting=th_weighting_function.unweighted,
            equivalent_duration=4.0,
            start_sample=0,
            stop_sample=8,
            leq_mode=LeqDurationMode.steady_state,
        )
        self.assertAlmostEqual(84.5, leq_steady_state, delta=0.5)

        leq_steady_state2 = th_steady_state.equivalent_level(
            weighting=th_weighting_function.unweighted,
            equivalent_duration=th_steady_state.duration,
            leq_mode=LeqDurationMode.steady_state,
        )
        self.assertAlmostEqual(84.5, leq_steady_state2, delta=0.5)

        leq_steady_state_8hr = th_steady_state.equivalent_level(
            weighting=th_weighting_function.unweighted,
            leq_mode=LeqDurationMode.steady_state,
            equivalent_duration=8 * 3600,
            exposure_duration=8 * 3600
        )
        self.assertAlmostEqual(84.5, leq_steady_state_8hr, delta=0.5)

        leq_steady_state_4hr = th_steady_state.equivalent_level(
            weighting=th_weighting_function.unweighted,
            leq_mode=LeqDurationMode.steady_state,
            equivalent_duration=8 * 3600,
            exposure_duration=4 * 3600
        )
        self.assertAlmostEqual(84.5 - 3, leq_steady_state_4hr, delta=0.5)

        leq_transient_avg = th_transient.equivalent_level(
            weighting=th_weighting_function.unweighted,
            leq_mode=LeqDurationMode.steady_state
        )
        self.assertAlmostEqual(
            84.5 - 3 + 10 * np.log10(th_transient.duration / 8 / 3600),
            leq_transient_avg,
            delta=0.5
        )

    def test_spectrogram_array_decibels(self):
        th = TimeHistory.load(test_Timehistory.time_history_example())

        self.assertEqual(np.shape(th.spectrogram_array_decibels), (len(th.times), len(th.frequencies)))

    def test_concatenate(self):
        th1 = TimeHistory.load(test_Timehistory.time_history_example_steady_state())
        th2 = TimeHistory.load(test_Timehistory.time_history_example_steady_state())

        th3 = th1.concatenate(th2, normalize_time=True)
        self.assertTrue(np.array_equal(th1.frequencies, th3.frequencies))
        self.assertEqual(th1.integration_time, th3.integration_time)
        self.assertEqual(th1.duration + th2.duration, th3.duration)
        self.assertEqual(th1.header, th3.header)
        for i in range(len(th1.times)):
            self.assertEqual(th1.spectra[i], th3.spectra[i])
        for i in range(len(th2.times)):
            self.assertEqual(th2.spectra[i], th3.spectra[len(th1.times) + i])

    def test_trim(self):
        th = TimeHistory.load(test_Timehistory.time_history_example_steady_state())

        trimmed_th = th.trim()
        self.assertEqual(th.times[0], trimmed_th.times[0])
        self.assertEqual(th.times[-1], trimmed_th.times[-1])
        self.assertEqual(th.integration_time, trimmed_th.integration_time)
        self.assertEqual(th.header, trimmed_th.header)

        trimmed_th = th.trim(end_time=th.times[-1]-1)
        self.assertEqual(th.times[0], trimmed_th.times[0])
        self.assertEqual(th.times[-1]-1, trimmed_th.times[-1])
        self.assertEqual(th.integration_time, trimmed_th.integration_time)
        self.assertEqual(th.header, trimmed_th.header)

        trimmed_th = th.trim(start_time=th.times[0] + 1)
        self.assertEqual(th.times[0] + 1, trimmed_th.times[0])
        self.assertEqual(th.times[-1], trimmed_th.times[-1])
        self.assertEqual(th.integration_time, trimmed_th.integration_time)
        self.assertEqual(th.header, trimmed_th.header)

        trimmed_th = th.trim(start_time=th.times[0] + 1, end_time=th.times[-1]-1)
        self.assertEqual(th.times[0] + 1, trimmed_th.times[0])
        self.assertEqual(th.times[-1]-1, trimmed_th.times[-1])
        self.assertEqual(th.integration_time, trimmed_th.integration_time)
        self.assertEqual(th.header, trimmed_th.header)

        t0 = th.times[0]+1
        t1 = th.times[-1]-1
        th.trim(start_time=th.times[0] + 1, end_time=th.times[-1] - 1, inplace=True)
        self.assertEqual(t0, th.times[0])
        self.assertEqual(t1, th.times[-1])

    def test_interp(self):
        a = test_Timehistory.time_history_example_steady_state()
        th = TimeHistory.load(a)

        times = th.times.astype(np.float64) + 0.1

        interpolated_th = th.interp(times)


class test_Narrowband_TimeHistory(unittest.TestCase):
    @staticmethod
    def out_TOB_example():
        from pathlib import Path

        return str(Path(__file__).parents[1]) + "/Test Data/Spectral/nb_to_fob.csv"

    def make_waveform(self, f: float = 1000, fs: float = 48000):
        sample_rate = fs
        max_frequency = f
        self.amplitude_dB = 94
        amplitude_rms = 10 ** (self.amplitude_dB / 20) * 2e-5
        x = np.arange(0, 2, 1 / sample_rate)
        y = amplitude_rms * np.sqrt(2) * np.sin(2 * np.pi * max_frequency * x)

        return Waveform(y, sample_rate=sample_rate, start_time=0.0)

    def test_constructor(self):
        wfm = self.make_waveform()
        integration_time = 0.25
        th = NarrowbandTimeHistory(wfm, integration_time=integration_time)

        self.assertIsNotNone(th.waveform)
        self.assertEqual(8192, th.fft_size)
        self.assertEqual(0.25 * 48000, th.waveform_sample_size)
        self.assertIsNotNone(th.integration_time)
        self.assertEqual(0.25, th.integration_time)
        self.assertIsNone(th._spectra)
        self.assertIsNone(th._times)
        self.assertEqual(1 / integration_time, th.time_history_sample_rate)
        self.assertEqual(2, th.duration)

        t = np.arange(0, 2, 0.25)
        for i in range(len(t)):
            self.assertEqual(t[i], th.times[i])
            self.assertAlmostEqual(94, th.overall_a_weighted_level[i], delta=1e-2)

    def test_overall_levels(self):
        wfm = self.make_waveform()
        th = NarrowbandTimeHistory(wfm)
        self.assertEqual(2 / 0.25, len(th.spectra))

        for i in range(len(th.spectra)):
            self.assertAlmostEqual(wfm.overall_level(0.25)[i], th.spectra[i].overall_level, delta=1e0)
            self.assertAlmostEqual(wfm.overall_level(0.25, weighting=WeightingFunctions.a_weighted)[i],
                                   th.spectra[i].overall_a_weighted_level, delta=1e0)

    def test_to_fob_spectra(self):
        wfm = self.make_waveform()
        nb_th = NarrowbandTimeHistory(wfm)
        self.assertEqual(2 / 0.25, len(nb_th.spectra))

        fob_th = nb_th.to_logarithmic_band_time_history()

        for i in range(len(fob_th.spectra)):
            self.assertEqual(31, len(fob_th.spectra[i].frequencies))
            self.assertAlmostEqual(wfm.overall_level(0.25)[i], fob_th.spectra[i].overall_level, delta=1e0)
            self.assertAlmostEqual(wfm.overall_level(0.25, weighting=WeightingFunctions.a_weighted)[i],
                                   fob_th.spectra[i].overall_a_weighted_level, delta=1e0)

    def test_nb_from_standard_bin_file(self):
        wfm = StandardBinaryFile(test_Timehistory.std_bin_example())
        nb_th = NarrowbandTimeHistory(wfm, integration_time=1.0)
        self.assertEqual(nb_th.times[0], wfm.start_time)
        self.assertAlmostEqual(nb_th.duration, wfm.duration, delta=1.0)

    def test_to_logarithmic_band_time_history_save_load(self):
        wfm = StandardBinaryFile(test_Timehistory.std_bin_example())
        self.assertIsNotNone(wfm._header)
        nb_th = NarrowbandTimeHistory(wfm, integration_time=1.0)
        self.assertIsNotNone(nb_th.header)
        tob_th = nb_th.to_logarithmic_band_time_history()
        self.assertIsNotNone(tob_th._header)
        tob_th.save(self.out_TOB_example())

        tob_th_test = TimeHistory.load(self.out_TOB_example())
        self.assertNotEqual(0, len(tob_th_test._header))
        self.assertEqual(int(tob_th_test._header['HEADER SIZE']), len(wfm._header) + 1)
        self.assertAlmostEqual(tob_th_test.duration, wfm.duration, delta=1.0)
        os.remove(self.out_TOB_example())

    def test_fifty_pct_overlap(self):
        #   Define a waveform with a 94 dB and 114 dB amplitude
        wfm94 = Waveform.generate_tone()
        wfm114 = Waveform.generate_tone(amplitude_db=114)

        #   Now create a new waveform that is the concatenation of the two signals
        samples = np.concatenate([wfm94.samples, wfm114.samples])
        wfm = Waveform(pressures=samples, sample_rate=wfm94.sample_rate, start_time=0)

        #   Now build the FrameBuilder class that will create the subsets
        frames = FrameBuilder(wfm, 0.5, 1)

        self.assertEqual(3, frames.complete_frame_count)

        spec = NarrowbandTimeHistory(wfm, frames, fft_size=4096)

        self.assertEqual(3, len(spec.spectra))
        self.assertAlmostEqual(94, spec.spectra[0].overall_level, places=4)
        self.assertAlmostEqual(114, spec.spectra[2].overall_level, places=4)
        self.assertAlmostEqual(10*np.log10(0.5*np.power(10.0, 94.0/10.0)+0.5*np.power(10.0, 114.0/10.0)),
                               spec.spectra[1].overall_level, delta=5e-1)

        frames = FrameBuilder(wfm, 0.75, 1)
        self.assertEqual(5, frames.complete_frame_count)

        spec = NarrowbandTimeHistory(wfm, frames, fft_size=4096)

        self.assertEqual(5, len(spec.spectra))
        self.assertAlmostEqual(94, spec.spectra[0].overall_level, places=4)
        self.assertAlmostEqual(114, spec.spectra[4].overall_level, places=4)
        self.assertAlmostEqual(
            10 * np.log10(0.75 * np.power(10.0, 94.0 / 10.0) + 0.25 * np.power(10.0, 114.0 / 10.0)),
            spec.spectra[1].overall_level, delta=5e-1
            )
        self.assertAlmostEqual(
            10 * np.log10(0.5 * np.power(10.0, 94.0 / 10.0) + 0.5 * np.power(10.0, 114.0 / 10.0)),
            spec.spectra[2].overall_level, delta=5e-1
        )
        self.assertAlmostEqual(
            10 * np.log10(0.25 * np.power(10.0, 94.0 / 10.0) + 0.75 * np.power(10.0, 114.0 / 10.0)),
            spec.spectra[3].overall_level, delta=5e-1
        )

        self.assertAlmostEqual(94, spec.spectra[0].waveform.overall_level(), places=4)
        self.assertAlmostEqual(
            10 * np.log10(0.25 * np.power(10.0, 94.0 / 10.0) + 0.75 * np.power(10.0, 114.0 / 10.0)),
            spec.spectra[3].waveform.overall_level(), delta=5e-2
        )


class test_LogarithmicBandTimeHistory(unittest.TestCase):
    @staticmethod
    def pink_noise_file():
        from pathlib import Path

        return str(
            Path(__file__).parents[1]) + '/Test Data/audio_files/Files/canonical wave file/pink_44100Hz_32bit.wav'

    def make_waveform(self, f: float = 1000, fs: float = 48000):
        sample_rate = fs
        max_frequency = f
        self.amplitude_dB = 94
        amplitude_rms = 10 ** (self.amplitude_dB / 20) * 2e-5
        x = np.arange(0, 2, 1 / sample_rate)
        y = amplitude_rms * np.sqrt(2) * np.sin(2 * np.pi * max_frequency * x)

        return Waveform(y, sample_rate=sample_rate, start_time=0.0)

    def test_constructor(self):
        wfm = self.make_waveform()
        th = LogarithmicBandTimeHistory(wfm)

        self.assertIsNotNone(th.waveform)
        self.assertEqual(0.25 * 48000, th.waveform_sample_size)
        self.assertIsNotNone(th.integration_time)
        self.assertEqual(0.25, th.integration_time)
        self.assertEqual(3, th.bandwidth)
        self.assertEqual(10, th.start_frequency)
        self.assertEqual(10000, th.stop_frequency)
        self.assertIsNone(th._spectra)
        self.assertIsNone(th._times)
        self.assertEqual(2, th.duration)

        t = np.arange(0, 2, 0.25)
        for i in range(len(t)):
            self.assertEqual(t[i], th.times[i])

    def test_overall_levels(self):

        #   process the waveform as a tone
        wfm = self.make_waveform()

        # Test 1 Octave
        th = LogarithmicBandTimeHistory(wfm, integration_time=1, fob_band_width=1)
        self.assertEqual(2, len(th.spectra))
        for i in range(len(th.spectra)):
            self.assertAlmostEqual(wfm.overall_level(1)[i], th.spectra[i].overall_level, delta=1e-1)
            self.assertAlmostEqual(wfm.overall_level(1, weighting=WeightingFunctions.a_weighted)[i],
                                   th.spectra[i].overall_a_weighted_level, delta=1e0)

        # Test 1/3 Octave
        th = LogarithmicBandTimeHistory(wfm, integration_time=1, fob_band_width=3)
        self.assertEqual(2, len(th.spectra))
        for i in range(len(th.spectra)):
            self.assertAlmostEqual(wfm.overall_level(1)[i], th.spectra[i].overall_level, delta=1e-1)
            self.assertAlmostEqual(wfm.overall_level(1, weighting=WeightingFunctions.a_weighted)[i],
                                   th.spectra[i].overall_a_weighted_level, delta=1e0)

        # Test 1/6 Octave
        th = LogarithmicBandTimeHistory(wfm, integration_time=1, fob_band_width=6)
        self.assertEqual(2, len(th.spectra))
        for i in range(len(th.spectra)):
            self.assertAlmostEqual(wfm.overall_level(1)[i], th.spectra[i].overall_level, delta=3e-1)
            self.assertAlmostEqual(wfm.overall_level(1, weighting=WeightingFunctions.a_weighted)[i],
                                   th.spectra[i].overall_a_weighted_level, delta=1e0)

        # Test 1/12 Octave
        th = LogarithmicBandTimeHistory(wfm, integration_time=1, fob_band_width=12)
        self.assertEqual(2, len(th.spectra))
        for i in range(len(th.spectra)):
            self.assertAlmostEqual(wfm.overall_level(1)[i], th.spectra[i].overall_level, delta=3e-1)
            self.assertAlmostEqual(wfm.overall_level(1, weighting=WeightingFunctions.a_weighted)[i],
                                   th.spectra[i].overall_a_weighted_level, delta=1e0)

        # Test 1/24 Octave
        th = LogarithmicBandTimeHistory(wfm, integration_time=1, fob_band_width=24)
        self.assertEqual(2, len(th.spectra))
        for i in range(len(th.spectra)):
            self.assertAlmostEqual(wfm.overall_level(1)[i], th.spectra[i].overall_level, delta=3e-1)
            self.assertAlmostEqual(wfm.overall_level(1, weighting=WeightingFunctions.a_weighted)[i],
                                   th.spectra[i].overall_a_weighted_level, delta=1.5e0)

        #   Now process the noise waveform
        wfm = WaveFile(test_LogarithmicBandTimeHistory.pink_noise_file())
        wfm = wfm.trim(0, 2 * wfm.sample_rate)

        th = LogarithmicBandTimeHistory(wfm, integration_time=1, fob_band_width=1)
        self.assertEqual(2, len(th.spectra))

        for i in range(len(th.spectra)):
            self.assertAlmostEqual(wfm.overall_level(1)[i], th.spectra[i].overall_level, delta=1.5e0)
            self.assertAlmostEqual(wfm.overall_level(1, weighting=WeightingFunctions.a_weighted)[i],
                                   th.spectra[i].overall_a_weighted_level, delta=1e0)

        th = LogarithmicBandTimeHistory(wfm, integration_time=1, fob_band_width=3)
        self.assertEqual(2, len(th.spectra))

        for i in range(len(th.spectra)):
            self.assertAlmostEqual(wfm.overall_level(1)[i], th.spectra[i].overall_level, delta=2.0e0)
            self.assertAlmostEqual(wfm.overall_level(1, weighting=WeightingFunctions.a_weighted)[i],
                                   th.spectra[i].overall_a_weighted_level, delta=2.0e0)

        th = LogarithmicBandTimeHistory(wfm, integration_time=1, fob_band_width=6)
        self.assertEqual(2, len(th.spectra))

        for i in range(len(th.spectra)):
            self.assertAlmostEqual(wfm.overall_level(1)[i], th.spectra[i].overall_level, delta=2.5)
            self.assertAlmostEqual(wfm.overall_level(1, weighting=WeightingFunctions.a_weighted)[i],
                                   th.spectra[i].overall_a_weighted_level, delta=2.5e0)

        th = LogarithmicBandTimeHistory(wfm, integration_time=1, fob_band_width=12)
        self.assertEqual(2, len(th.spectra))

        for i in range(len(th.spectra)):
            self.assertAlmostEqual(wfm.overall_level(1)[i], th.spectra[i].overall_level, delta=2.5)
            self.assertAlmostEqual(wfm.overall_level(1, weighting=WeightingFunctions.a_weighted)[i],
                                   th.spectra[i].overall_a_weighted_level, delta=2.5e0)

        th = LogarithmicBandTimeHistory(wfm, integration_time=1, fob_band_width=24)
        self.assertEqual(2, len(th.spectra))

        for i in range(len(th.spectra)):
            self.assertAlmostEqual(wfm.overall_level(1)[i], th.spectra[i].overall_level, delta=2.5)
            self.assertAlmostEqual(wfm.overall_level(1, weighting=WeightingFunctions.a_weighted)[i],
                                   th.spectra[i].overall_a_weighted_level, delta=2.5e0)

    def test_get_features(self):
        wfm = self.make_waveform(1000)

        th = LogarithmicBandTimeHistory(wfm)

        dataset = th.get_features()

        self.assertEqual(th.duration / th.integration_time, dataset.shape[0])
        time = 1
        level = 3 + 30
        temporal = 10+1+12
        sound_quality = 4
        spectral = 10
        harmonic = 10
        self.assertEqual(time+level+temporal+sound_quality+spectral+harmonic, dataset.shape[1])

    # def test_get_features_from_loaded_file(self):
    #     th = TimeHistory.load(test_Timehistory.time_history_example_transient())
    #     features = th.get_features
    #     for j in range(len(features)):
    #         for i in range(26):
    #             self.assertTrue(np.isnan(features.iloc[j, i]))

    def test_fundamental_freq(self):
        wfm = WaveFile(test_Timehistory.multitone_noise_change())
        t_hist = LogarithmicBandTimeHistory(wfm, integration_time=0.25)
        self.assertAlmostEqual(t_hist.fundamental_frequency[0], 620, delta=10)
        self.assertAlmostEqual(t_hist.fundamental_frequency[-1], 950, delta=10)

    def test_fifty_pct_overlap(self):
        #   Define a waveform with a 94 dB and 114 dB amplitude
        wfm94 = Waveform.generate_tone()
        wfm114 = Waveform.generate_tone(amplitude_db=114)

        #   Now create a new waveform that is the concatenation of the two signals
        samples = np.concatenate([wfm94.samples, wfm114.samples])
        wfm = Waveform(pressures=samples, sample_rate=wfm94.sample_rate, start_time=0)

        #   Now build the FrameBuilder class that will create the subsets
        frames = FrameBuilder(wfm, 0.5, 1)

        self.assertEqual(3, frames.complete_frame_count)

        spec = LogarithmicBandTimeHistory(wfm, frames=frames)

        self.assertEqual(3, len(spec.spectra))

        w1 = 1
        w2 = 0
        for i in range(frames.complete_frame_count):
            lvl = 10 * np.log10( w1 * 10**(94/10) + w2 * 10**(114/10))
            self.assertAlmostEqual(lvl, spec.spectra[i].overall_level, delta=0.55)

            w1 -= 1 - frames.overlap_percentage
            w2 += 1 - frames.overlap_percentage

        frames = FrameBuilder(wfm, 0.75, 1)
        self.assertEqual(5, frames.complete_frame_count)

        spec = LogarithmicBandTimeHistory(wfm, frames)

        self.assertEqual(5, len(spec.spectra))

        w1 = 1
        w2 = 0
        for i in range(frames.complete_frame_count):
            lvl = 10 * np.log10(w1 * 10 ** (94 / 10) + w2 * 10 ** (114 / 10))
            self.assertAlmostEqual(lvl, spec.spectra[i].overall_level, delta=1.0)

            w1 -= 1 - frames.overlap_percentage
            w2 += 1 - frames.overlap_percentage

    def test_AudioDeepFake(self):
        import soundfile as sf
        import pytimbre.spectral.time_histories as spectrogram
        import pytimbre.waveform as waveform
        from pathlib import Path

        filename = str(Path(__file__).parents[1]) + '/Test Data/audio_files/Files/PA_T_0053999.flac'

        samples, fs = sf.read(filename)

        wfm = waveform.Waveform(samples, fs, 0).resample(48000)

        spec = spectrogram.LogarithmicBandTimeHistory(wfm, FrameBuilder(wfm, 0.5, 1), f1=6300)
        features = spec.get_features(include_speech_features=False,
                                     include_harmonic_features=False,
                                     include_temporal_features=False,
                                     include_sq_metrics=True)
        t = 0


class test_SpectrogramTimeHistory(unittest.TestCase):
    @staticmethod
    def out_TOB_example():
        from pathlib import Path

        return str(Path(__file__).parents[1]) + "/Test Data/Spectral/nb_to_fob.csv"

    def make_waveform(self, f: float = 1000, fs: float = 48000):
        sample_rate = fs
        max_frequency = f
        self.amplitude_dB = 94
        amplitude_rms = 10 ** (self.amplitude_dB / 20) * 2e-5
        x = np.arange(0, 2, 1 / sample_rate)
        y = amplitude_rms * np.sqrt(2) * np.sin(2 * np.pi * max_frequency * x)

        return Waveform(y, sample_rate=sample_rate, start_time=0.0)

    def test_constructor(self):
        wfm = self.make_waveform()
        integration_time = 0.25
        th = Spectrogram(wfm, fft_size=4096)

        self.assertIsNotNone(th.waveform)
        self.assertEqual(4096, th.fft_size)
        self.assertEqual(0.25 * 48000, th.waveform_sample_size)
        self.assertIsNotNone(th.integration_time)
        self.assertEqual(0.25, th.integration_time)
        self.assertIsNotNone(th.spectra)
        self.assertIsNotNone(th.times)
        self.assertNotEqual(1 / integration_time, th.time_history_sample_rate)
        self.assertEqual(1 / np.mean(np.diff(th.times_past_midnight)), th.time_history_sample_rate)
        self.assertEqual(wfm.duration, th.duration)

        dt = 0.042666666666666666
        t = np.arange(dt, wfm.duration-dt, dt)
        for i in range(len(t)):
            self.assertAlmostEqual(t[i], th.times[i], delta=1e-6)
            self.assertAlmostEqual(94, th.overall_a_weighted_level[i], delta=2)

    @staticmethod
    def make_spectrogram():
        """
        Generate the test spectrogram data for comparison of data between the Matlab and Python implementation of the
        code

        Returns
        -------
        tuple of time, frequency, distribution
        """

        t = np.arange(0, 10, 0.0058)
        f = np.arange(0, 0.5, 1 / 4096)

        distribution_filename = str(pathlib.Path(__file__).parents[1]) + "/Test Data/Spectral/distribution.csv"
        distribution = np.loadtxt(distribution_filename, delimiter=',')

        return t, f, distribution.transpose()

    @staticmethod
    def make_waveform():
        """
        Generate the acoustic waveform that was created in Matlab to make the various spectral features

        Parameters
        ----------
        None

        Returns
        -------
        Waveform of a 1000 Hz signal 10s in duration.
        """
        fs = 48000
        f = 1000
        w = 2 * np.pi * f
        t = np.arange(0, 10, 1 / fs)
        signal = 0.75 * np.sin(w * t)

        return Waveform(signal, fs, 0)

    def test_spectral_centroid(self):
        t, f, level = test_SpectrogramTimeHistory.make_spectrogram()
        s = Spectrogram.from_data(level, f, t, levels_as_pressure=True)

        dataset = pd.read_csv(
            str(pathlib.Path(__file__).parents[1]) + "/Test Data/Spectral/spectral_features.csv",
            index_col=False
        )

        self.assertEqual(dataset.shape[0], len(s.spectra))

        for i in range(dataset.shape[0]):
            self.assertAlmostEqual(dataset.iloc[i, 0], s.spectra[i].spectral_centroid, delta=1e-8)

    def test_spectral_spread(self):
        t, f, level = test_SpectrogramTimeHistory.make_spectrogram()
        s = Spectrogram.from_data(level, f, t, levels_as_pressure=True)

        dataset = pd.read_csv(
            str(pathlib.Path(__file__).parents[1]) + "/Test Data/spectral_features.csv",
            index_col=False
        )

        self.assertEqual(dataset.shape[0], len(s.spectra))

        for i in range(dataset.shape[0]):
            self.assertAlmostEqual(dataset.iloc[i, 1], s.spectra[i].spectral_spread, delta=1e-8)

    def test_spectral_skewness(self):
        t, f, level = test_SpectrogramTimeHistory.make_spectrogram()
        s = Spectrogram.from_data(level, f, t, levels_as_pressure=True)

        dataset = pd.read_csv(
            str(pathlib.Path(__file__).parents[1]) + "/Test Data/spectral_features.csv",
            index_col=False
        )
        data = dataset['Skew'].values

        self.assertEqual(dataset.shape[0], len(s.spectra))

        for i in range(dataset.shape[0]):
            self.assertAlmostEqual(data[i], s.spectra[i].spectral_skewness, delta=1e-8)

    def test_spectral_kurtosis(self):
        t, f, level = test_SpectrogramTimeHistory.make_spectrogram()
        s = Spectrogram.from_data(level, f, t, levels_as_pressure=True)

        dataset = pd.read_csv(
            str(pathlib.Path(__file__).parents[1]) + "/Test Data/spectral_features.csv",
            index_col=False
        )
        data = dataset['Kurtosis'].values

        self.assertEqual(dataset.shape[0], len(s.spectra))

        for i in range(dataset.shape[0]):
            self.assertAlmostEqual(data[i], s.spectra[i].spectral_kurtosis, delta=1e-8)

    def test_spectral_slope(self):
        t, f, level = test_SpectrogramTimeHistory.make_spectrogram()
        s = Spectrogram.from_data(level, f, t, levels_as_pressure=True)

        dataset = pd.read_csv(
            str(pathlib.Path(__file__).parents[1]) + "/Test Data/spectral_features.csv",
            index_col=False
        )
        data = dataset['Slope'].values

        self.assertEqual(dataset.shape[0], len(s.spectra))

        for i in range(dataset.shape[0]):
            self.assertAlmostEqual(data[i], s.spectra[i].spectral_slope, delta=1e-8)

    def test_spectral_decrease(self):
        t, f, level = test_SpectrogramTimeHistory.make_spectrogram()
        s = Spectrogram.from_data(level, f, t, levels_as_pressure=True)

        dataset = pd.read_csv(
            str(pathlib.Path(__file__).parents[1]) + "/Test Data/spectral_features.csv",
            index_col=False
        )
        data = dataset['Decrease'].values

        self.assertEqual(dataset.shape[0], len(s.spectra))

        for i in range(dataset.shape[0]):
            self.assertAlmostEqual(data[i], s.spectra[i].spectral_decrease, delta=1e-8)

    def test_spectral_roll_off(self):
        t, f, level = test_SpectrogramTimeHistory.make_spectrogram()
        s = Spectrogram.from_data(level, f, t)

        dataset = pd.read_csv(
            str(pathlib.Path(__file__).parents[1]) + "/Test Data/spectral_features.csv",
            index_col=False
        )
        data = dataset['Rolloff'].values

        self.assertEqual(dataset.shape[0], len(s.spectra))

        for i in range(dataset.shape[0]):
            self.assertTrue(s.spectra[i].spectral_roll_off * 48000 >= 1000)

    def test_spectral_energy(self):

        from pytimbre.spectral.spectra import SpectrumByFFT

        wfm = Waveform.generate_tone(1000, amplitude_db=91)
        s = SpectrumByFFT(wfm, fft_size=4096)

        self.assertAlmostEqual(0.5, s.spectral_energy, delta=1e-2)

        wfm = Waveform.generate_tone(1000, amplitude_db=94)
        s = SpectrumByFFT(wfm, fft_size=4096)

        self.assertAlmostEqual(1, s.spectral_energy, delta=1e-2)

        wfm = Waveform.generate_tone(1000, amplitude_db=97)
        s = SpectrumByFFT(wfm, fft_size=4096)

        self.assertAlmostEqual(2, s.spectral_energy, delta=1e-2)

    def test_spectral_flatness(self):
        t, f, level = test_SpectrogramTimeHistory.make_spectrogram()
        s = Spectrogram.from_data(level, f, t, levels_as_pressure=True)

        dataset = pd.read_csv(
            str(pathlib.Path(__file__).parents[1]) + "/Test Data/spectral_features.csv",
            index_col=False
        )
        data = dataset['Flatness'].values

        self.assertEqual(dataset.shape[0], len(s.spectra))

        for i in range(dataset.shape[0]):
            self.assertAlmostEqual(data[i], s.spectra[i].spectral_flatness, delta=1e-8)

    def test_spectral_crest(self):
        t, f, level = test_SpectrogramTimeHistory.make_spectrogram()
        s = Spectrogram.from_data(level, f, t, levels_as_pressure=True)

        dataset = pd.read_csv(
            str(pathlib.Path(__file__).parents[1]) + "/Test Data/spectral_features.csv",
            index_col=False
        )
        data = dataset['Crest'].values

        self.assertEqual(dataset.shape[0], len(s.spectra))

        for i in range(dataset.shape[0]):
            self.assertAlmostEqual(data[i], s.spectra[i].spectral_crest, delta=1e-7)

    def test_overall_level(self):
        wfm = Waveform.generate_tone()

        spectrogram = Spectrogram(wfm)

        lf = spectrogram.overall_level
        for i in range(len(lf)):
            self.assertAlmostEqual(94, lf[i], delta=2.2)

    def test_from_data(self):
        t = np.arange(0, 20) * 0.25
        f = 1000 * 2**((np.arange(10, 41)-30)/3)
        level = np.ones((len(t), len(f))) * 1

        tmh = TimeHistory.from_data(level, f, t, levels_as_pressure=True)

        self.assertEqual(len(tmh.times), len(t))
        self.assertEqual(len(f), len(tmh.frequencies))
        self.assertEqual((len(t), len(f)), tmh.spectrogram_array_decibels.shape)

        for i in range(len(t)):
            self.assertEqual(tmh.times[i], t[i])
            for j in range(len(f)):
                self.assertEqual(tmh.frequencies[j], f[j])
                self.assertEqual(level[i, j], tmh.spectra[i].pressures_pascals[j])

        level = np.ones((len(t), len(f))) * 94
        tmh = TimeHistory.from_data(level, f, t, levels_as_pressure=False)

        self.assertEqual(len(tmh.times), len(t))
        self.assertEqual(len(f), len(tmh.frequencies))
        self.assertEqual((len(t), len(f)), tmh.spectrogram_array_decibels.shape)

        for i in range(len(t)):
            self.assertEqual(tmh.times[i], t[i])
            for j in range(len(f)):
                self.assertEqual(tmh.frequencies[j], f[j])
                self.assertAlmostEqual(1.0, tmh.spectra[i].pressures_pascals[j], places=2)
