from unittest import TestCase
import numpy as np
from pytimbre.spectral.fractional_octave_band import FractionalOctaveBandTools
import pandas as pd
from pathlib import Path
from pytimbre.waveform import Waveform


class TestFractionalOctaveBand(TestCase):
    @staticmethod
    def filtering_waveform():
        filename = str(Path(__file__).parents[1]) + '/Test Data/Spectral/fractional_octave_filtering_third_octaves_' + \
                   'downsampling_waveform_samples.csv'
        samples = np.loadtxt(filename)

        return Waveform(pressures=samples, sample_rate=48000, start_time=0)

    @staticmethod
    def one_sixth_fc():
        return str(
            Path(__file__).parents[1]) + '/Test Data/Spectral/one-sixth-ob-fc.csv'

    @staticmethod
    def one_twelvth_fc():
        return str(
            Path(__file__).parents[1]) + '/Test Data/Spectral/one-twelvth-ob-fc.csv'

    @staticmethod
    def fractional_octave_center_frequency_analysis():
        return str(
            Path(__file__).parents[1]) + '/Test Data/Spectral/Fractional Octave Bandwidth Center Frequency ' \
                                         'analysis.xlsx'

    @staticmethod
    def fractional_octave_shapes():
        return str(
            Path(__file__).parents[1]) + '/Test Data/Spectral/Hsq band definition.xlsx'

    @staticmethod
    def minimum_audible_field():
        return str(Path(__file__).parents[1]) + '/Test Data/Spectral/MinimumAudibleField.csv'

    def test_exact_band_number(self):
        self.assertEqual(0., FractionalOctaveBandTools.exact_band_number(1, 1000))
        self.assertEqual(30., FractionalOctaveBandTools.exact_band_number(3, 1000))

        # Test against selected exact mid-band frequency values as tabulated in IEC 61260-1:2014
        # NOTE:
        test_bands = np.arange(0, 6)

        test_frequencies_one_ob = np.float64([1000., 2000., 4000., 8000., 16000., 32000.])
        for i in range(len(test_bands)):
            self.assertAlmostEqual(
                float(test_bands[i]),
                FractionalOctaveBandTools.exact_band_number(resolution=1, frequency=test_frequencies_one_ob[i]),
                delta=0.1
            )

        test_frequencies_one_third_ob = np.float64([1., 1.26, 1.58, 2., 2.51, 3.16])
        for i in range(len(test_bands)):
            self.assertAlmostEqual(
                float(test_bands[i]),
                FractionalOctaveBandTools.exact_band_number(resolution=3, frequency=test_frequencies_one_third_ob[i]),
                delta=0.2
            )

        test_frequencies_one_sixth_ob = np.float64([1059.3, 1188.5, 1333.5, 1496.2, 1678.8, 1883.6])
        for i in range(len(test_bands)):
            self.assertAlmostEqual(
                float(test_bands[i]),
                FractionalOctaveBandTools.exact_band_number(resolution=6, frequency=test_frequencies_one_sixth_ob[i]),
                delta=0.1
            )

        test_frequencies_one_twelfth_ob = np.float64([1029.2, 1090.2, 1154.8, 1223.2, 1295.7, 1372.5])
        for i in range(len(test_bands)):
            self.assertAlmostEqual(
                float(test_bands[i]),
                FractionalOctaveBandTools.exact_band_number(resolution=12, frequency=test_frequencies_one_twelfth_ob[i]),
                delta=0.1
            )


    def test_center_frequency(self):
        self.assertEqual(1000, FractionalOctaveBandTools.center_frequency(1, 0))
        self.assertEqual(1000, FractionalOctaveBandTools.center_frequency(3, 30))

        # Test 1/12 Octave Bands
        labView_values = np.loadtxt(TestFractionalOctaveBand.one_twelvth_fc())

        for band in range(-80, 41):
            expected = float(labView_values[80 + band])
            actual = FractionalOctaveBandTools.center_frequency(12, band)

            percent_error = abs(expected - actual) / expected
            self.assertTrue(percent_error < 3e-2)

        # Test 1/24 Octave Bands
        test_values = pd.read_excel(TestFractionalOctaveBand.fractional_octave_center_frequency_analysis(),
                                    sheet_name='one-24-ob-fc', skiprows=79, header=None)

        for i in range(test_values.shape[0] - 1):
            expected = test_values.iloc[i, 0]
            actual = FractionalOctaveBandTools.center_frequency(24, test_values.iloc[i, 3])

            percent_error = abs(expected - actual) / expected
            self.assertTrue(percent_error < 5e-2, msg='Error at {} with expected {} and actual {}'.format(
                i, expected, actual
            ))

        # Test 1/6 Octave Bands
        test_values = pd.read_excel(TestFractionalOctaveBand.fractional_octave_center_frequency_analysis(),
                                    sheet_name='one-6-ob', header=None)

        for i in range(test_values.shape[0]):
            expected = test_values.iloc[i, 0]
            actual = FractionalOctaveBandTools.center_frequency(6, test_values.iloc[i, 1])

            percent_error = abs(expected - actual) / expected
            self.assertTrue(percent_error < 3e-2, msg='Error at {} with expected {} and actual {}'.format(
                i, expected, actual
            ))

        # Test 1/3 Octave Bands
        test_bands = np.arange(0, 6)
        test_frequencies_one_third_ob = np.float64([1., 1.25, 1.6, 2., 2.5, 3.12])
        for i in range(len(test_bands)):
            self.assertAlmostEqual(
                test_frequencies_one_third_ob[i],
                FractionalOctaveBandTools.center_frequency(resolution=3, band=test_bands[i]),
                delta=0.05 * test_frequencies_one_third_ob[i]
            )

        # Test 1 Octave Bands
        test_bands = np.arange(0, 6)
        test_frequencies_one_ob = np.float64([1000., 2000., 4000., 8000., 16000., 32000.])
        for i in range(len(test_bands)):
            self.assertAlmostEqual(
                test_frequencies_one_ob[i],
                FractionalOctaveBandTools.center_frequency(resolution=1, band=test_bands[i]),
                delta=0.05 * test_frequencies_one_ob[i]
            )

    def test_lower_frequency(self):
        self.assertAlmostEqual(707.107, FractionalOctaveBandTools.lower_frequency(1, 0), delta=1e-2)
        self.assertAlmostEqual(890.899, FractionalOctaveBandTools.lower_frequency(3, 30), delta=1e-2)

        # Test 1/3 Octave Bands
        test_bands = np.arange(0, 6)
        test_frequencies_one_third_ob = np.float64([1., 1.25, 1.6, 2., 2.5, 3.12]) / (2 ** (1 / 6))
        for i in range(len(test_bands)):
            self.assertAlmostEqual(
                test_frequencies_one_third_ob[i],
                FractionalOctaveBandTools.lower_frequency(resolution=3, band=test_bands[i]),
                delta=0.05 * test_frequencies_one_third_ob[i]
            )

        # Test 1 Octave Bands
        test_bands = np.arange(0, 6)
        test_frequencies_one_ob = np.float64([1000., 2000., 4000., 8000., 16000., 32000.]) / (2 ** (1 / 2))
        for i in range(len(test_bands)):
            self.assertAlmostEqual(
                test_frequencies_one_ob[i],
                FractionalOctaveBandTools.lower_frequency(resolution=1, band=test_bands[i]),
                delta=0.05 * test_frequencies_one_ob[i]
            )

    def test_upper_frequency(self):
        self.assertAlmostEqual(1414.214, FractionalOctaveBandTools.upper_frequency(1, 0), delta=1e-2)
        self.assertAlmostEqual(1122.462, FractionalOctaveBandTools.upper_frequency(3, 30), delta=1e-2)

        # Test 1/3 Octave Bands
        test_bands = np.arange(0, 6)
        test_frequencies_one_third_ob = np.float64([1., 1.25, 1.6, 2., 2.5, 3.12]) * 2 ** (1 / 6)
        for i in range(len(test_bands)):
            self.assertAlmostEqual(
                test_frequencies_one_third_ob[i],
                FractionalOctaveBandTools.upper_frequency(resolution=3, band=test_bands[i]),
                delta=0.05 * test_frequencies_one_third_ob[i]
            )

        # Test 1 Octave Bands
        test_bands = np.arange(0, 6)
        test_frequencies_one_ob = np.float64([1000., 2000., 4000., 8000., 16000., 32000.]) * 2 ** (1 / 2)
        for i in range(len(test_bands)):
            self.assertAlmostEqual(
                test_frequencies_one_ob[i],
                FractionalOctaveBandTools.upper_frequency(resolution=1, band=test_bands[i]),
                delta=0.05 * test_frequencies_one_ob[i]
            )

    def test_bandwidth(self):
        self.assertAlmostEqual(707.107, FractionalOctaveBandTools.band_width(1, 0), delta=1e-2)
        self.assertAlmostEqual(231.563, FractionalOctaveBandTools.band_width(3, 30), delta=1e-2)

    def test_frequencies(self):
        freq_list = FractionalOctaveBandTools.frequencies(10, 40, 3)
        self.assertEqual(31, len(freq_list))
        self.assertEqual(1000, freq_list[30 - 10])

    def test_min_audible_field(self):
        contents = pd.read_csv(TestFractionalOctaveBand.minimum_audible_field())
        for line_index in range(contents.shape[0]):
            f = contents.iloc[line_index, 0]
            expected = contents.iloc[line_index, -1]
            self.assertAlmostEqual(expected, FractionalOctaveBandTools.min_audible_field(f), delta=1e-2)

    def test_get_min_audible_field_tob(self):
        db = FractionalOctaveBandTools.get_min_audible_fields()
        self.assertEqual(31, len(db))

        for line_index in range(31):
            self.assertAlmostEqual(FractionalOctaveBandTools.min_audible_field(
                FractionalOctaveBandTools.tob_frequencies()[line_index]),
                                   db[line_index], delta=1e-2)

    def test_tob_frequencies_ansi(self):
        freq = FractionalOctaveBandTools.tob_frequencies_ansi()
        self.assertEqual(31, len(freq))
        self.assertEqual(1000, freq[20])

    def test_tob_frequencies(self):
        freq = FractionalOctaveBandTools.tob_frequencies()
        self.assertEqual(31, len(freq))
        self.assertEqual(1000, freq[20])

    def test_tob_to_erb(self):
        corrections = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -0.984947508,
                       -1.886499608, -2.675271289, -3.356668021, -3.938319912, -4.429410575, -4.839970954, -5.180235728,
                       -5.460125156, -5.688877443, -5.874826467, -6.025301356, -6.146617156, -6.244126428, -6.322306421,
                       -6.384862746, -6.434836444, -6.474706281, -6.506481782]

        n = 0

        for f in FractionalOctaveBandTools.tob_frequencies():
            actual = FractionalOctaveBandTools.tob_to_erb(f, 0)
            self.assertAlmostEqual(corrections[n], actual, delta=1e-5)

            n += 1

    def test_center_frequency_to_erb(self):
        corrections = [25.76245795, 26.03861314, 26.38654688, 26.82491591, 27.37722628, 28.07309375, 28.94983182,
                       30.05445257,
                       31.4461875, 33.19966364, 35.40890513, 38.192375, 41.69932728, 46.11781027, 51.68475, 58.69865455,
                       67.53562054, 78.6695, 92.6973091, 110.3712411,
                       132.639, 160.6946182, 196.0424821, 240.578, 296.6892364, 367.3849643, 456.456, 568.6784728,
                       710.0699286, 888.212, 1112.656946]

        for i in range(0, len(corrections), 1):
            self.assertAlmostEqual(corrections[i], FractionalOctaveBandTools.center_frequency_to_erb(
                FractionalOctaveBandTools.tob_frequencies()[i]), delta=1e-5)

    def test_frequencies_ansi_preferred(self):

        self.assertListEqual(list(FractionalOctaveBandTools.frequencies_ansi_preferred(63, 1000, 1)),
                             [63, 125, 250, 500, 1000])

        self.assertListEqual(list(FractionalOctaveBandTools.frequencies_ansi_preferred(100, 1000, 3)),
                             [100, 125, 160, 200, 250, 315, 400, 500, 630, 800, 1000])

    def test_filter_shape(self):
        sheet_name = 'Calculation of shape 1.25khz'
        data = pd.read_excel(TestFractionalOctaveBand.fractional_octave_shapes(), sheet_name)

        shape = FractionalOctaveBandTools.filter_shape(3, 1250, data.iloc[:, 0])

        self.assertEqual(data.shape[0], len(shape))

        for i in range(data.shape[0]):
            self.assertAlmostEqual(data.iloc[i, -2], shape[i], delta=1e-6)

        sheet_name = 'Calculation of shape 1khz'
        data = pd.read_excel(TestFractionalOctaveBand.fractional_octave_shapes(), sheet_name)

        shape = FractionalOctaveBandTools.filter_shape(3, 1000, data.iloc[:, 0])

        self.assertEqual(data.shape[0], len(shape))

        for i in range(data.shape[0]):
            self.assertAlmostEqual(data.iloc[i, -2], shape[i], delta=1e-6)

    def test_plot_filtershape(self):
        manual = False
        if manual:
            import matplotlib.pyplot as plt

            fig, ax = plt.subplots()

            fc = 1000
            freq, lo, hi = FractionalOctaveBandTools.ansi_band_limits(0, fc, 3)

            sheet_name = 'Calculation of shape 1khz'
            data = pd.read_excel(TestFractionalOctaveBand.fractional_octave_shapes(), sheet_name)

            shape = FractionalOctaveBandTools.filter_shape(3, fc, data.iloc[:, 0])

            ax.semilogx(freq, 10 ** (lo / 20), 'k')
            ax.semilogx(freq, 10 ** (hi / 20), 'b')
            ax.semilogx(data.iloc[:, 0], shape, 'r')

            ax.set_xlim([fc * 2 ** (-6), fc * 2 ** 6])
            ax.set_ylim([0, 1.2])

            plt.show()

    def test_get_fob_frequencies(self):
        f = FractionalOctaveBandTools.get_frequency_array(band_width=1, f0=16, f1=500)
        freq_list = [16, 31.5, 63, 125, 250, 500]
        for i in range(len(freq_list)):
            self.assertAlmostEqual(f[i], freq_list[i], delta=1)

        f = FractionalOctaveBandTools.get_frequency_array(3, 10, 10000)

        self.assertEqual(len(FractionalOctaveBandTools.tob_frequencies()), len(f))

        for i in range(len(f)):
            self.assertAlmostEqual(FractionalOctaveBandTools.tob_frequencies()[i], f[i], delta=1)

        bw = 2
        self.assertRaises(ValueError, FractionalOctaveBandTools.get_frequency_array, bw, 16, 500)

    def test_filter_third_octaves_downsample(self):
        wfm = TestFractionalOctaveBand.filtering_waveform()

        pressure = 99.33787236410447
        frequencies = np.array([25., 31.5, 40., 50., 63., 80., 100., 125., 160.,
                                200., 250., 315., 400., 500., 630., 800., 1000.,
                                1250., 1600., 2000., 2500., 3150., 4000., 5000.,
                                6300., 8000., 10000., 12500.])
        band_values = np.array([57.43843881, 58.80390027, 60.57869843, 62.65165217, 65.70647935, 74.47102347,
                                99.28420867, 78.22839943, 63.01461133, 57.31013202, 53.19043464, 49.5996553,
                                46.22862647, 43.03275634, 39.94956724, 36.85520705, 33.84245455, 30.8900821,
                                27.84361239, 24.8963606, 22.02351224, 18.97421235, 16.09666208, 13.34392178,
                                10.34740048, 7.63073234, 5.09245523, 2.84539824])

        total_pressure, band_pressure, f = FractionalOctaveBandTools.filter_third_octaves_downsample(wfm)

        self.assertEqual(len(frequencies), len(f))
        for i in range(len(f)):
            self.assertEqual(frequencies[i], f[i])
            self.assertAlmostEqual(band_values[i], band_pressure[i], delta=1e-6)

        self.assertAlmostEqual(pressure, total_pressure, delta=1e-6)

    def test_weighted_bark_level(self):

        wfm = TestFractionalOctaveBand.filtering_waveform()
        mean_lvl, weighted_lvl = FractionalOctaveBandTools.weighted_bark_level(wfm, 0, 70)

        self.assertAlmostEqual(255.5437627154398, mean_lvl, delta=6e-1)
        self.assertAlmostEqual(255.55389242014652, weighted_lvl, delta=6e-1)