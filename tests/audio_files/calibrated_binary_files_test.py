from unittest import TestCase
from pytimbre.audio_files.calibrated_binary_files import MeasurementLogFile, CalibratedBinaryFile
from pytimbre.audio_files.wavefile import WaveFile
from datetime import datetime
from pathlib import Path
import os.path


class Test_ALARMS_Format(TestCase):
    #   ------------------------------ Path definitions for files ------------------------------------------------------

    @staticmethod
    def AFRL_Legacy_bin_ID000():
        return str(Path(__file__).parents[1]) + "/Test Data/audio_files/Files/AFRL Acoustic Recorder/ID000_000.bin"

    @staticmethod
    def AFRL_Legacy_log_2():
        return str(Path(__file__).parents[1]) + "/Test Data/audio_files/Files/AFRL Acoustic Recorder/ID0001_log_A5.txt"

    @staticmethod
    def AFRL_Legacy_bin_ID008():
        return str(Path(__file__).parents[1]) + "/Test Data/audio_files/Files/AFRL Acoustic Recorder/ID008_000.bin"

    @staticmethod
    def AFRL_Legacy_log_ID000():
        return str(Path(__file__).parents[1]) + "/Test Data/audio_files/Files/AFRL Acoustic Recorder/ID000_log.txt"

    @staticmethod
    def AFRL_Legacy_log_ID008():
        return str(Path(__file__).parents[1]) + "/Test Data/audio_files/Files/AFRL Acoustic Recorder/ID008_log.txt"

    @staticmethod
    def test_AFRL_Recorder_log():
        return str(Path(__file__).parents[1]) + "/Test Data/audio_files/Files/AFRL Acoustic Recorder/" \
                                                "ID00110_AFRL_log.txt"

    @staticmethod
    def test_AFRL_Recorder_bin():
        return str(Path(__file__).parents[1]) + "/Test Data/audio_files/Files/AFRL Acoustic Recorder/" \
                                                "ID00110_00001.AFRL.bin"

    @staticmethod
    def test_AFRL_Recorder_log_11():
        return str(Path(__file__).parents[1]) + "/Test Data/audio_files/Files/AFRL Acoustic Recorder/" \
                                                "ID00111_AFRL_log.txt"

    @staticmethod
    def test_AFRL_Recorder_bin_11():
        return str(Path(__file__).parents[1]) + "/Test Data/audio_files/Files/AFRL Acoustic Recorder/" \
                                                "ID00111_00001.AFRL.bin"

    @staticmethod
    def test_AFRL_Recorder_log_13():
        return str(Path(__file__).parents[1]) + "/Test Data/audio_files/Files/AFRL Acoustic Recorder/" \
                                                "ID00113_AFRL_log.txt"

    @staticmethod
    def test_AFRL_Recorder_bin_13():
        return str(Path(__file__).parents[1]) + "/Test Data/audio_files/Files/AFRL Acoustic Recorder/" \
                                                "ID00113_00001.AFRL.bin"

    #   --------------------------- MeasurementLogFile tests -----------------------------------------------------------

    def test_MeasurementLogFile_constructor(self):
        log = MeasurementLogFile(self.AFRL_Legacy_log_ID000())

        self.assertEqual("ID000", log.filename)
        self.assertEqual(51, log.channel_number)
        self.assertEqual(204800, log.sample_rate)

        log_new = MeasurementLogFile(self.AFRL_Legacy_log_2())

        self.assertEqual("ID0001", log_new.filename)
        self.assertEqual(3, log_new.channel_number)
        self.assertEqual(51200.0, log_new.sample_rate)
        self.assertEqual(datetime(2021, 3, 28, 18, 7, 58, 900000), log_new.start_time)

    def test_AFRL_Legacy_logfile_start_time(self):
        log = MeasurementLogFile(self.AFRL_Legacy_log_ID000())

        dt = datetime(2021, 8, 27, 0, 24, 52, 183000)

        self.assertEqual(dt.year, log.start_time.year)
        self.assertEqual(dt.month, log.start_time.month)
        self.assertEqual(dt.day, log.start_time.day)
        self.assertEqual(dt.hour, log.start_time.hour)
        self.assertEqual(dt.minute, log.start_time.minute)
        self.assertEqual(dt.second, log.start_time.second)
        self.assertEqual(dt.microsecond, log.start_time.microsecond)

    def test_afrl_acoustic_recorder_MeasurementLogFile(self):
        log = MeasurementLogFile(self.test_AFRL_Recorder_log())

        self.assertEqual("ID00110_AFRL_log.txt", log.filename)
        self.assertEqual(2022, log.start_time.year)
        self.assertEqual(3, log.start_time.month)
        self.assertEqual(17, log.start_time.day)
        self.assertEqual(15, log.start_time.hour)
        self.assertEqual(16, log.start_time.minute)
        self.assertEqual(6, log.start_time.second)
        self.assertEqual(729000, log.start_time.microsecond)
        self.assertEqual(0, log.gmt_offset)
        self.assertEqual(4, log.channel_number)
        self.assertEqual(51200, log.sample_rate)
        self.assertEqual(5120, log.block_size)
        self.assertEqual("Recording test", log.comments)
        self.assertAlmostEqual(6.802419, log.record_duration, delta=1e-5)

    def test_Legacy_CalibratedBinaryFile_constructor(self):
        abs_path = self.AFRL_Legacy_bin_ID000()
        dir_path = os.path.dirname(abs_path)

        log_path = self.AFRL_Legacy_log_ID000()

        log = MeasurementLogFile(log_path)
        f1 = CalibratedBinaryFile(log, abs_path)

        self.assertIsNotNone(f1)
        self.assertAlmostEqual(136.71587583061506, f1.peak_level, places=8)
        self.assertAlmostEqual(1.4672021484375, f1.peak_time - f1.times[0], places=8)
        self.assertEqual(log.sample_rate, f1.sample_rate)

        f2 = CalibratedBinaryFile(log, dir_path, "ID000", "000")
        self.assertIsNotNone(f2)
        self.assertAlmostEqual(136.71587583061506, f2.peak_level, places=8)
        self.assertAlmostEqual(1.4672021484375, f2.peak_time - f1.times[0], places=8)
        self.assertEqual(log.sample_rate, f2.sample_rate)

        f2 = CalibratedBinaryFile(log, dir_path, "ID000", "CH000")
        self.assertIsNotNone(f2)
        self.assertAlmostEqual(136.71587583061506, f2.peak_level, places=8)
        self.assertAlmostEqual(1.4672021484375, f2.peak_time - f1.times[0], places=8)
        self.assertEqual(log.sample_rate, f2.sample_rate)

        f3 = CalibratedBinaryFile(log, dir_path, 0, 0)
        self.assertIsNotNone(f3)
        self.assertAlmostEqual(136.71587583061506, f3.peak_level, places=8)
        self.assertAlmostEqual(1.4672021484375, f3.peak_time - f1.times[0], places=8)
        self.assertEqual(log.sample_rate, f3.sample_rate)

    def test_Legacy_CalibratedBinaryFile_peak_level(self):
        abs_path = self.AFRL_Legacy_bin_ID000()

        log_path = self.AFRL_Legacy_log_ID000()

        log = MeasurementLogFile(log_path)
        f1 = CalibratedBinaryFile(log, abs_path)
        self.assertAlmostEqual(136.71587583061506, f1.peak_level, places=8)

    def test_Legacy_CalibratedBinaryFile_peak_time(self):
        abs_path = self.AFRL_Legacy_bin_ID000()

        log_path = self.AFRL_Legacy_log_ID000()

        log = MeasurementLogFile(log_path)
        f1 = CalibratedBinaryFile(log, abs_path)
        self.assertAlmostEqual(1.4672021484375, f1.peak_time - f1.times[0], places=8)

    def test_Legacy_CalibratedBinaryFile_sample_rate(self):
        abs_path = self.AFRL_Legacy_bin_ID000()

        log_path = self.AFRL_Legacy_log_ID000()

        log = MeasurementLogFile(log_path)
        f1 = CalibratedBinaryFile(log, abs_path)
        self.assertEqual(log.sample_rate, f1.sample_rate)

    def test_afrl_acoustic_recorder_bin(self):

        log = MeasurementLogFile(self.test_AFRL_Recorder_log_11())

        bin = CalibratedBinaryFile(log, self.test_AFRL_Recorder_bin_11())

        self.assertIsNotNone(bin)
        self.assertEqual(6.30125, bin.duration)
        self.assertEqual(6.30125 * 51200, len(bin.samples))

    def test_afrl_acoustic_recorder_bin_2(self):

        import os.path

        log = MeasurementLogFile(self.test_AFRL_Recorder_log_13())

        bin = CalibratedBinaryFile(log, self.test_AFRL_Recorder_bin_13())

        wfm = WaveFile(samples=bin.samples, fs=bin.fs, time=bin.start_time)
        wfm.normalized = True

        filename = os.path.dirname(self.test_AFRL_Recorder_log_13()) + "/test.wav"
        if os.path.exists(filename):
            os.remove(filename)

        wfm.save(filename)





