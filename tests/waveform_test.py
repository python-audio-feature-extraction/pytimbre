import datetime
import unittest

from numpy.distutils.command.build import build

from pytimbre.waveform import Waveform, FrameBuilder, from_db_to_pa
from pytimbre.waveform import WeightingFunctions, TrimmingMethods, WindowingMethods, LeqDurationMode, AnalysisMethod
from pytimbre.audio_files.ansi_standard_formatted_files import StandardBinaryFile
from pytimbre.spectral.time_histories import LogarithmicBandTimeHistory as band_hist
from pytimbre.temporal.temporal_metrics import EquivalentLevel
from pytimbre.spectral.spectra import SpectrumByFFT
import numpy as np
from pathlib import Path
import pandas as pd


class TestFromDBToPa(unittest.TestCase):
    def test_from_db_to_pa(self):
        leq = EquivalentLevel(
            equivalent_pressure_decibels=114,
            equivalent_duration=datetime.timedelta(seconds=1)
        )
        self.assertAlmostEqual(leq.equivalent_pressure_pascals, 10.0, delta=0.1)


class TestWaveform(unittest.TestCase):
    @staticmethod
    def test_bin_write():
        return str(Path(__file__).parents[0]) + "/Test Data/audio_files/Files/standard_bin_file_write" \
                                                "/test_standard_bin_file.bin"

    @staticmethod
    def filtering_waveform():
        filename = str(
            Path(__file__).parents[1]) + '/tests/Test Data/Spectral/fractional_octave_filtering_third_octaves_' + \
                   'downsampling_waveform_samples.csv'
        samples = np.loadtxt(filename)

        return Waveform(pressures=samples, sample_rate=48000, start_time=0)

    @staticmethod
    def get_wfm(f: float = 1000):
        fs = 48000
        w = 2 * np.pi * f
        t = np.arange(0, 10, 1 / fs)

        wfm = Waveform(0.75 * np.sin(w * t), fs, 0.0)

        return wfm

    @staticmethod
    def get_wfm_2(f: float = 1000):
        f = 100
        w = 2 * np.pi * f
        fs = 48000
        t = np.arange(0, 2, 1 / fs)
        signal = np.cos(w * t)

        wfm = Waveform(signal, fs, 0.0)

        return wfm

    @staticmethod
    def std_bin_file_c130_to_wav():
        return str(Path(__file__).parents[1]) + "/Test Data/waveformdata/files/after landing_MIP Rack.bin"

    @staticmethod
    def low_pass_filtered_data():
        return str(Path(__file__).parents[1]) + "/_Test Data/waveformdata/100 hz filtered signal.csv"

    @staticmethod
    def std_bin_file_transient():
        return str(Path(__file__).parents[1]) + "/tests/Test Data/audio_files/calibrated_aversive_spkall_mic1.bin"

    @staticmethod
    def std_bin_file_transient_2():
        return str(Path(__file__).parents[1]) + "/tests/Test Data/audio_files/calibrated_aversive_combo_spkall_mic1.bin"

    @staticmethod
    def multitone_noise_600hz():
        return str(Path(__file__).parents[1]) + "/tests/Test Data/audio_files/multi_tone_simulated_noise_600hz_f0.wav"

    @staticmethod
    def multitone_noise_1000hz():
        return str(Path(__file__).parents[1]) + "/tests/Test Data/audio_files/multi_tone_simulated_noise_1000hz_f0.wav"

    def test_constructor(self):
        wfm = TestWaveform.get_wfm()

        self.assertEqual(480000, len(wfm.samples))
        self.assertEqual(10, wfm.duration)
        self.assertEqual(48000, wfm.sample_rate)
        self.assertEqual(0.0, wfm.time0)
        self.assertEqual(len(wfm.samples), len(wfm.times))
        self.assertIsNone(wfm.forward_coefficients)
        self.assertIsNone(wfm.reverse_coefficients)
        self.assertIsNotNone(wfm.signal_envelope)
        self.assertIsNotNone(wfm.normal_signal_envelope)
        self.assertEqual(12, wfm.coefficient_count)
        self.assertEqual(0.0029, wfm.hop_size_seconds)
        self.assertEqual(0.0232, wfm.window_size_seconds)
        self.assertEqual(5, wfm.cutoff_frequency)
        self.assertEqual(0.15, wfm.centroid_threshold)
        self.assertEqual(0.4, wfm.effective_duration_threshold)

    def test_trim_samples_method(self):
        wfm = TestWaveform.get_wfm(f=1000.34159)

        s0 = 0
        s1 = 1000

        wfm1 = wfm.trim(s0, s1, method=TrimmingMethods.samples)
        self.assertEqual(1000, len(wfm1.samples))

        wfm2 = wfm.trim(1, s1 + 1, method=TrimmingMethods.samples)
        self.assertEqual(1000, len(wfm2.samples))
        for i in range(len(wfm2.samples)):
            self.assertEqual(wfm.samples[i + 1], wfm2.samples[i])

    def test_trim_time_absolute(self):
        wfm = TestWaveform.get_wfm(f=1000.34159)
        wfm.start_time = datetime.datetime(year=1, month=1, day=1, hour=0, minute=0, second=10)

        wfm1 = wfm.trim(s0=11.0, s1=12.0, method=TrimmingMethods.times_absolute)
        self.assertEqual(datetime.datetime(year=1, month=1, day=1, hour=0, minute=0, second=11), wfm1.time0)
        for i in range(len(wfm1.samples)):
            self.assertEqual(wfm.samples[i + int(1.0 * wfm.sample_rate)], wfm1.samples[i])

    def test_trim_time_relative(self):
        wfm = TestWaveform.get_wfm(f=1000.34159)
        wfm.start_time = datetime.datetime(year=1, month=1, day=1, hour=0, minute=0, second=10)

        start_time_rel_wfm_start_s = 1.0
        end_time_rel_wfm_start_s = 2.0
        wfm1 = wfm.trim(
            s0=start_time_rel_wfm_start_s,
            s1=end_time_rel_wfm_start_s,
            method=TrimmingMethods.times_relative
        )
        self.assertEqual(datetime.datetime(year=1, month=1, day=1, hour=0, minute=0, second=11), wfm1.time0)
        for i in range(len(wfm1.samples)):
            self.assertEqual(wfm.samples[i + int(start_time_rel_wfm_start_s * wfm.sample_rate)], wfm1.samples[i])

    def test_apply_window(self):
        import scipy.signal.windows

        wfm = self.get_wfm(1000)

        wfm1 = wfm.apply_window(WindowingMethods.tukey, 0.05)
        window = scipy.signal.windows.tukey(len(wfm.samples), 0.05)
        self.assertEqual(len(wfm.samples), len(wfm1.samples))
        for i in range(len(wfm.samples)):
            self.assertAlmostEqual(wfm.samples[i] * window[i], wfm1.samples[i], delta=1e-6)

        wfm1 = wfm.apply_window(WindowingMethods.tukey, 0.5)
        window = scipy.signal.windows.tukey(len(wfm.samples), 0.5)
        self.assertEqual(len(wfm.samples), len(wfm1.samples))
        for i in range(len(wfm.samples)):
            self.assertAlmostEqual(wfm.samples[i] * window[i], wfm1.samples[i], delta=1e-6)

        wfm1 = wfm.apply_window(WindowingMethods.rectangular)
        window = scipy.signal.windows.tukey(len(wfm.samples), 0.0)
        self.assertEqual(len(wfm.samples), len(wfm1.samples))
        for i in range(len(wfm.samples)):
            self.assertAlmostEqual(wfm.samples[i] * window[i], wfm1.samples[i], delta=1e-6)

        wfm1 = wfm.apply_window(WindowingMethods.hanning)
        window = scipy.signal.windows.tukey(len(wfm.samples), 1)
        self.assertEqual(len(wfm.samples), len(wfm1.samples))
        for i in range(len(wfm.samples)):
            self.assertAlmostEqual(wfm.samples[i] * window[i], wfm1.samples[i], delta=1e-6)

        wfm1 = wfm.apply_window(WindowingMethods.hamming)
        window = scipy.signal.windows.hamming(len(wfm.samples))
        self.assertEqual(len(wfm.samples), len(wfm1.samples))
        for i in range(len(wfm.samples)):
            self.assertAlmostEqual(wfm.samples[i] * window[i], wfm1.samples[i], delta=1e-6)

    def test_apply_iir_filter(self):
        import scipy.signal

        wfm = self.get_wfm(100)

        b, a = scipy.signal.butter(4, 1000 / wfm.sample_rate / 2, btype='low', analog=False, output='ba')

        wfm1 = wfm.apply_lowpass(1000, 4)

        b = np.array(
            [1.55517217808043e-05, 6.22068871232173e-05, 9.33103306848260e-05, 6.22068871232173e-05,
             1.55517217808043e-05]
        )

        a = np.array([1, -3.65806030240188, 5.03143353336761, -3.08322830175882, 0.710103898341587])

        self.assertEqual(5, len(wfm.forward_coefficients))
        for i in range(5):
            self.assertAlmostEqual(b[i], wfm.forward_coefficients[i], delta=1e-10)

        self.assertEqual(5, len(wfm.reverse_coefficients))
        for i in range(5):
            self.assertAlmostEqual(a[i], wfm.reverse_coefficients[i], delta=1e-10)

        wfm2 = wfm.apply_iir_filter(b, a)

        b = np.array(
            [1.55517217808043e-05, 6.22068871232173e-05, 9.33103306848260e-05, 6.22068871232173e-05,
             1.55517217808043e-05]
        )

        a = np.array([1, -3.65806030240188, 5.03143353336761, -3.08322830175882, 0.710103898341587])

        self.assertEqual(5, len(wfm.forward_coefficients))
        for i in range(5):
            self.assertAlmostEqual(b[i], wfm.forward_coefficients[i], delta=1e-10)

        self.assertEqual(5, len(wfm.reverse_coefficients))
        for i in range(5):
            self.assertAlmostEqual(a[i], wfm.reverse_coefficients[i], delta=1e-10)

        self.assertEqual(len(wfm1.samples), len(wfm2.samples))

        for i in range(len(wfm1.samples)):
            self.assertAlmostEqual(wfm1.samples[i], wfm2.samples[i], delta=1e-2)

    def test_AC_Filter_design(self):
        # Check AC_design_filter design against outputs from matlab version
        fs = 2e5

        Ba_matlab = [0.032084206336563, -0.064168412667423, -0.0320842063536748, 0.128336825357664, -0.032084206325157,
                     -0.0641684126902407, 0.0320842063422688]
        Aa_matlab = [1, -5.32939721339437, 11.7682035394565, -13.7759185980543, 9.01251831975431, -3.12310892469402,
                     0.447702876935291]
        Bc_matlab = [0.0260099630889772, 4.62329257443558e-12, -0.0520199261825801, -4.62329257443599e-12,
                     0.0260099630936028]
        Ac_matlab = [1, -3.35568854501294, 4.17126593340454, -2.27533221962382, 0.459754874493225]

        # Call ac_filter_design from python at fs
        coeffA, coeffC = Waveform.AC_Filter_Design(fs)

        # Check all coeffecients across a and c filters
        for i in range(len(Aa_matlab)):
            self.assertAlmostEqual(Ba_matlab[i], coeffA[0][i], delta=1e-8)
            self.assertAlmostEqual(Aa_matlab[i], coeffA[1][i], delta=1e-8)

        for i in range(len(Ac_matlab)):
            self.assertAlmostEqual(Bc_matlab[i], coeffC[0][i], delta=1e-8)
            self.assertAlmostEqual(Ac_matlab[i], coeffC[1][i], delta=1e-8)

    def test_apply_a_weight(self):
        wfm = self.get_wfm_2()

        wfm2 = wfm.apply_a_weight()

        self.assertEqual(
            wfm.overall_level(weighting=WeightingFunctions.a_weighted),
            wfm2.overall_level()
        )

    def test_apply_c_weight(self):
        wfm = self.get_wfm_2()

        wfm2 = wfm.apply_c_weight()

        self.assertEqual(
            wfm.overall_level(weighting=WeightingFunctions.c_weighted),
            wfm2.overall_level()
        )

    def test_low_pass(self):
        #   Define a signal

        f = 100
        w = 2 * np.pi * f
        fs = 48000
        t = np.arange(0, 2, 1 / fs)
        signal = np.cos(w * t)

        wfm = Waveform(signal, fs, 0.0)

        wfm.apply_lowpass(1000)

        b = np.array(
            [1.55517217808043e-05, 6.22068871232173e-05, 9.33103306848260e-05, 6.22068871232173e-05,
             1.55517217808043e-05]
        )

        a = np.array([1, -3.65806030240188, 5.03143353336761, -3.08322830175882, 0.710103898341587])

        self.assertEqual(5, len(wfm.forward_coefficients))
        for i in range(5):
            self.assertAlmostEqual(b[i], wfm.forward_coefficients[i], delta=1e-10)

        self.assertEqual(5, len(wfm.reverse_coefficients))
        for i in range(5):
            self.assertAlmostEqual(a[i], wfm.reverse_coefficients[i], delta=1e-10)

    def test_is_calibration(self):
        wfm0 = TestWaveform.get_wfm()

        self.assertTrue(wfm0.is_calibration()[0])

        wfm0 = TestWaveform.get_wfm(250)

        self.assertTrue(wfm0.is_calibration()[0])

        wfm0 = TestWaveform.get_wfm(200)

        self.assertFalse(wfm0.is_calibration()[0])

    def test_get_features(self):
        wfm = TestWaveform.get_wfm()

        features = wfm.get_features()
        self.assertAlmostEqual(0.030208333333333, features['attack'], delta=1e-4)
        self.assertAlmostEqual(0.137937500000000, features['decrease'], delta=1e-4)
        self.assertAlmostEqual(10, features['release'], delta=1e-4)
        self.assertAlmostEqual(-1.050813362350563, features['log_attack'], delta=1e-4)
        self.assertAlmostEqual(10.284324504585928, features['attack slope'], delta=2e-1)
        self.assertAlmostEqual(-0.0768171115635973, features['decrease slope'], delta=1e-6)
        self.assertAlmostEqual(5.032777294219399, features['temporal centroid'], delta=1e-4)
        self.assertAlmostEqual(9.937354166666667, features['effective duration'], delta=1e-4)
        self.assertAlmostEqual(4.092080415748489e-04, features['amplitude modulation'], delta=1e-6)
        self.assertAlmostEqual(0.065570535881231, features['frequency modulation'], delta=1e-4)
        self.assertEqual(12, features['auto-correlation'].shape[1])
        self.assertEqual(3446, features['auto-correlation'].shape[0])
        self.assertAlmostEqual(0.991427742278755, features['auto-correlation'][0, 0], delta=1e-6)
        self.assertAlmostEqual(-2.94422758033417e-05, features['auto-correlation'][-1, -1], delta=3e-5)
        self.assertEqual(3446, len(features['zero crossing rate']))
        self.assertAlmostEqual(2025.13464991023, features['zero crossing rate'][0], delta=1e-6)
        self.assertAlmostEqual(1982.04667863555, features['zero crossing rate'][-1], delta=1e-6)
        self.assertTrue('mfcc_00' in features.keys())

        wfm.is_impulsive = True
        wfm.impulse_analysis_method = AnalysisMethod.NO_A_DURATION_CORRECTIONS

        features = wfm.get_features()
        self.assertEqual(9, len(features))

    # def test_rms_envelope(self):
    #     #   Open the file containing the signal and the RMS envelope

    def test_attack(self):
        wfm = TestWaveform.get_wfm()

        self.assertAlmostEqual(0.030208333333333, wfm.attack, delta=1e-4)

    def test_decrease(self):
        wfm = TestWaveform.get_wfm()

        self.assertAlmostEqual(0.137937500000000, wfm.decrease, delta=1e-4)

    def test_release(self):
        wfm = TestWaveform.get_wfm()

        self.assertAlmostEqual(10, wfm.release, delta=1e-4)

    def test_log_attack(self):
        wfm = TestWaveform.get_wfm()

        self.assertAlmostEqual(-1.050813362350563, wfm.log_attack, delta=1e-4)

    def test_attack_slope(self):
        wfm = TestWaveform.get_wfm()

        self.assertAlmostEqual(10.284324504585928, wfm.attack_slope, delta=2e-1)

    def test_decrease_slope(self):
        wfm = TestWaveform.get_wfm()

        self.assertAlmostEqual(-0.0768171115635973, wfm.decrease_slope, delta=1e-6)

    def test_temporal_centroid(self):
        wfm = TestWaveform.get_wfm()

        self.assertAlmostEqual(5.032777294219399, wfm.temporal_centroid, delta=1e-4)

    def test_effective_duration(self):
        wfm = TestWaveform.get_wfm()

        self.assertAlmostEqual(9.937354166666667, wfm.effective_duration, delta=1e-4)

    def test_amplitude_modulation(self):
        wfm = TestWaveform.get_wfm()

        self.assertAlmostEqual(4.092080415748489e-04, wfm.amplitude_modulation, delta=1e-6)

    def test_frequency_modulation(self):
        wfm = TestWaveform.get_wfm()

        self.assertAlmostEqual(0.065570535881231, wfm.frequency_modulation, delta=1e-4)

    def test_auto_correlation(self):
        wfm = TestWaveform.get_wfm()

        self.assertEqual(12, wfm.auto_correlation.shape[1])
        self.assertEqual(3446, wfm.auto_correlation.shape[0])

        self.assertAlmostEqual(0.991427742278755, wfm.auto_correlation[0, 0], delta=1e-6)
        self.assertAlmostEqual(-2.94422758033417e-05, wfm.auto_correlation[-1, -1], delta=3e-5)

    def test_zero_crossing(self):
        wfm = TestWaveform.get_wfm()

        self.assertEqual(3446, len(wfm.zero_crossing_rate))

        self.assertAlmostEqual(2025.13464991023, wfm.zero_crossing_rate[0], delta=1e-6)
        self.assertAlmostEqual(1982.04667863555, wfm.zero_crossing_rate[-1], delta=1e-6)

    def test_if_Waveform_trim_returns_correct_duration(self):

        sample_rate = 2000
        x = np.linspace(0, 2 * np.pi, 1000)
        pressure = np.sqrt(2) * np.sin(x)
        f1 = Waveform(pressure, sample_rate, start_time=0)

        start_sample_trimmed = 0
        end_sample_trimmed = start_sample_trimmed + int(0.1 * f1.sample_rate)
        f2 = f1.trim(start_sample_trimmed, end_sample_trimmed)
        self.assertEqual(f2.duration, 0.1)

    def TestWaveform_time_setter(self):

        sample_rate = 2000
        x = np.linspace(0, 2 * np.pi, 1000)
        pressure = np.sqrt(2) * np.sin(x)

        f1 = Waveform(pressure, sample_rate, start_time=0)
        self.assertEqual(0.0, f1.times[0])
        self.assertEqual(0.0, f1.time0)
        self.assertEqual(0.0, f1.start_time)

        f1.start_time = 1
        self.assertEqual(1.0, f1.times[0])
        self.assertEqual(1.0, f1.time0)
        self.assertEqual(1.0, f1.start_time)

    def test_filtering_coefficients(self):
        #   Define a signal

        wfm = self.get_wfm(100)

        wfm.apply_lowpass(1000)

        b = np.array(
            [1.55517217808043e-05, 6.22068871232173e-05, 9.33103306848260e-05, 6.22068871232173e-05,
             1.55517217808043e-05]
        )

        a = np.array([1, -3.65806030240188, 5.03143353336761, -3.08322830175882, 0.710103898341587])

        self.assertEqual(5, len(wfm.forward_coefficients))
        for i in range(5):
            self.assertAlmostEqual(b[i], wfm.forward_coefficients[i], delta=1e-10)

        self.assertEqual(5, len(wfm.reverse_coefficients))
        for i in range(5):
            self.assertAlmostEqual(a[i], wfm.reverse_coefficients[i], delta=1e-10)

    def test_apply_bandpass(self):
        wfm = Waveform.generate_tone(250, amplitude_db=94)

        wfm.apply_bandpass(800, 1000, 4)

        # b = np.array([0.99999714952586,-3.99998859810344,5.99998289715516,-3.9998859810344,0.9999971495286])
        # a = np.array([1, -3.91448750483634, 5.74709974634780, -3.75064701042629, 0.918035868172305])

        # coefficient_scale = wfm.forward_coefficients[0] / b[0]

        self.assertEqual(9, len(wfm.forward_coefficients))
        # for i in range(5):
        #     self.assertAlmostEqual(b[i], wfm.forward_coefficients[i] / coefficient_scale, delta=1e-10)
        #     if i > 0:
        #         self.assertAlmostEqual(a[i], wfm.reverse_coefficients[i], delta=1)

    def test_trim2(self):
        """
        There was an issue with the generation of the subset of the waveform for the determination of the time history.
        The tests pass when the waveform is taken as a whole, but not using these subsets. So this tests the trimming
        to ensure that the subset is correctly determined.
        """

        wfm = Waveform.generate_tone(duration=10)

        integration_time = 0.1

        N = int(np.floor(wfm.duration / integration_time))

        s0 = 0

        for i in range(N):
            subset = wfm.trim(s0, s0 + int(np.floor(integration_time * wfm.sample_rate)), TrimmingMethods.samples)

            for j in range(s0, s0 + len(subset.samples)):
                self.assertAlmostEqual(subset.samples[j - s0], wfm.samples[j], places=5)

            s0 += len(subset.samples)

    def test_high_pass(self):
        #   Define a signal
        wfm = Waveform.generate_tone(250, amplitude_db=94)

        wfm.apply_highpass(1000)

        b = np.array([0.958141883111421, -3.83256753244568, 5.74885129866853, -3.83256753244568, 0.958141883111421])
        a = np.array([1, -3.91448750483634, 5.74709974634780, -3.75064701042629, 0.918035868172305])

        coefficient_scale = wfm.forward_coefficients[0] / b[0]

        self.assertEqual(5, len(wfm.forward_coefficients))
        for i in range(5):
            self.assertAlmostEqual(b[i], wfm.forward_coefficients[i] / coefficient_scale, delta=1e-10)
            if i > 0:
                self.assertAlmostEqual(a[i], wfm.reverse_coefficients[i], delta=1)

    def test_resample(self):
        wfm = self.get_wfm(250)

        wfm_down = wfm.resample(44100)

        self.assertEqual(441000, len(wfm_down.samples))

        wfm_up = wfm.resample(96000)

        self.assertEqual(960000, len(wfm_up.samples))

    # TODO - Add tests for waveform.add
    # def test_add_operator(self):
    #     self.assertTrue(False, "Not implemented")

    # def test_sound_quality_metrics(self):
    #     self.assertTrue(False, "Not implemented")
    #
    # def test_apply_tob_equalizer(self):
    #     self.assertTrue(False, "Not Implemented")

    def test_bandpass(self):
        wfm = self.get_wfm(1000)
        wfm.apply_bandpass(800, 1100)

        b = [7.28117247216156e-06, 0, -2.18435174164847e-05, 0, 2.18435174164847e-05, 0, -7.28117247216156e-06]
        a = [1, -5.87687197128807, 14.4349893473931, -18.9674687942193, 14.0619417177387, -5.57704759766939,
             0.924460583186420]

        self.assertEqual(len(b), len(wfm.forward_coefficients))
        for i in range(len(b)):
            self.assertAlmostEqual(b[i], wfm.forward_coefficients[i], delta=1e-6)

        self.assertEqual(len(a), len(wfm.forward_coefficients))
        for i in range(len(a)):
            self.assertAlmostEqual(a[i], wfm.reverse_coefficients[i], delta=1e-6)

    def test_apply_calibration(self):
        #   Test that the function returns a value error when the detected frequency is not what was specified
        calibration = Waveform.generate_tone(250, amplitude_db=75)
        test_wfm = Waveform.generate_noise()
        self.assertRaises(ValueError, test_wfm.apply_calibration, calibration, 114, 1000, False)

        calibration = Waveform.generate_tone(1000, amplitude_db=75)

        calibrated_wfm = test_wfm.apply_calibration(calibration, 114, 1000)
        self.assertAlmostEqual(test_wfm.overall_level()[0] + (114 - 75), calibrated_wfm.overall_level()[0], places=1)

    def test_gps_audio_signal_converter(self):
        filename = str(Path(__file__).parents[0]) + "/Test Data/audio_files/d31s2dr0520180625_0.wav"

        start_time = Waveform.gps_audio_signal_converter(filename)

        # 205057
        self.assertEqual(2018, start_time.year)
        self.assertEqual(6, start_time.month)
        self.assertEqual(25, start_time.day)
        self.assertEqual(20, start_time.hour)
        self.assertEqual(50, start_time.minute)
        self.assertEqual(56, start_time.second)

    def test_correlation(self):
        wfm = Waveform.generate_tone(1000)

        self.assertEqual(wfm.cross_correlation(wfm)[1], 0)

    def test_is_clipped(self):

        #   Build the signal that is not clipped...a white noise signal
        non_clipped = Waveform.generate_noise()

        #   Clip the samples
        clipped = Waveform(np.clip(non_clipped.samples, -0.5, 0.5), sample_rate=non_clipped.sample_rate, start_time=0)

        self.assertFalse(non_clipped.is_clipped)
        self.assertTrue(clipped.is_clipped)

    def test_noise_generation(self):

        wfm = Waveform.generate_noise()

        self.assertAlmostEqual(94, wfm.overall_level()[0], delta=1.5)
        # fig, ax = plt.subplots()
        # ax.plot(wfm.times, wfm.samples)
        #
        # plt.show()

    def test_concatenate(self):
        wfm1 = Waveform.generate_tone()
        wfm2 = Waveform.generate_tone()

        wfm3 = wfm1.concatenate(wfm2)

        self.assertEqual(2, wfm3.duration)

    def test_if_equivalent_level_returns_correct_magnitudes(self):

        waveform_duration = 2.0
        waveform_amplitude_db = 85.0
        wfm = Waveform.generate_noise(duration=waveform_duration, amplitude_db=waveform_amplitude_db)

        leq_trans_a = wfm.equivalent_level(
            WeightingFunctions.unweighted,
            leq_mode=LeqDurationMode.transient,
            equivalent_duration=waveform_duration
        )
        self.assertAlmostEqual(waveform_amplitude_db, leq_trans_a, delta=0.2)

        leq_trans_b = wfm.equivalent_level(
            WeightingFunctions.unweighted,
            leq_mode=LeqDurationMode.transient,
            equivalent_duration=waveform_duration * 2
        )
        self.assertAlmostEqual(waveform_amplitude_db - 3, leq_trans_b, delta=0.2)

        leq_steady_8hr = wfm.equivalent_level(
            WeightingFunctions.unweighted,
            leq_mode=LeqDurationMode.steady_state,
            equivalent_duration=8 * 3600,
            exposure_duration=8 * 3600
        )
        self.assertAlmostEqual(waveform_amplitude_db, leq_steady_8hr, delta=0.2)

        leq_steady_4hr = wfm.equivalent_level(
            WeightingFunctions.unweighted,
            leq_mode=LeqDurationMode.steady_state,
            equivalent_duration=8 * 3600,
            exposure_duration=4 * 3600
        )
        self.assertAlmostEqual(waveform_amplitude_db - 3, leq_steady_4hr, delta=0.2)

        waveform_duration = 1.0
        waveform_amplitude_db = 85.0
        wfm = Waveform.generate_noise(duration=waveform_duration, amplitude_db=waveform_amplitude_db)

        leq_trans_avg = wfm.equivalent_level(
            WeightingFunctions.unweighted,
            leq_mode=LeqDurationMode.steady_state
        )
        self.assertAlmostEqual(
            waveform_amplitude_db + 10 * np.log10(waveform_duration / 8 / 3600), leq_trans_avg,
            delta=0.2
        )

    def test_transient_attack(self):
        wfm = StandardBinaryFile(TestWaveform.std_bin_file_transient())
        wfm = wfm.trim(0, 1, TrimmingMethods.times_relative)
        t_hist = band_hist(a=wfm, fob_band_width=3, integration_time=0.25, f0=10, f1=10000)
        try:
            t_hist.get_features(True,True,True,True,True)

            self.assertTrue(True, msg='Calculated all metrics for the given waveform. Test passed!')
        except:
            self.assertFalse(True, msg='Failed to calculate features for given spectral time history.')

    def test_transient_attack_2(self):
        wfm = StandardBinaryFile(TestWaveform.std_bin_file_transient_2())
        wfm = wfm.trim(20.5, 21, TrimmingMethods.times_relative)
        t_hist = band_hist(a=wfm, fob_band_width=3, integration_time=0.25, f0=10, f1=10000)
        try:
            t_hist.get_features(True,True,True,True,True)
            self.assertTrue(True, msg='Calculated all metrics for the given waveform. Test passed!')
        except:
            self.assertFalse(True, msg='Failed to calculate features for given spectral time history.')

    def test_fundamental(self):
        wfm = Waveform.generate_tone(1900)
        self.assertAlmostEqual(1900, wfm.fundamental_frequency, delta=10)

    def test_is_continuous(self):
        wfm = Waveform.generate_tone()
        self.assertTrue(wfm.is_continuous)
        self.assertFalse(wfm.is_impulsive)

        wfm.is_continuous = False
        self.assertFalse(wfm.is_continuous)
        self.assertTrue(wfm.is_impulsive)

        wfm.is_impulsive = False
        self.assertTrue(wfm.is_continuous)
        self.assertFalse(wfm.is_impulsive)

        wfm.is_impulsive = True
        self.assertFalse(wfm.is_continuous)
        self.assertTrue(wfm.is_impulsive)

        wfm = Waveform(np.zeros((48000,)), 48000, 0, is_continuous_wfm=True)
        self.assertTrue(wfm.is_continuous)
        self.assertFalse(wfm.is_impulsive)

        wfm = Waveform(np.zeros((48000,)), 48000, 0, is_continuous_wfm=False)
        self.assertFalse(wfm.is_continuous)
        self.assertTrue(wfm.is_impulsive)

    def test_is_impulsive_apply_a_weight(self):
        wfm = Waveform.generate_friedlander(
            peak_level=170.7,
            a_duration=4e-4,
            duration=0.1,
            blast_time=0.05
        )
        wfm.is_impulsive = True
        sela1 = wfm.equivalent_level(
            weighting=WeightingFunctions.a_weighted,
            equivalent_duration=1.0,
            leq_mode=LeqDurationMode.transient
        )

        wfma = wfm.apply_a_weight()
        sela2 = wfma.equivalent_level(
            weighting=WeightingFunctions.unweighted,
            equivalent_duration=1.0,
            leq_mode=LeqDurationMode.transient
        )
        self.assertAlmostEqual(sela1, sela2, delta=0.3)

    def test_is_steady_state(self):
        wfm = Waveform.generate_tone()
        self.assertTrue(wfm.is_steady_state)
        self.assertFalse(wfm.is_transient)

        wfm.is_steady_state = False
        self.assertFalse(wfm.is_steady_state)
        self.assertTrue(wfm.is_transient)

        wfm.is_transient = False
        self.assertTrue(wfm.is_steady_state)
        self.assertFalse(wfm.is_transient)

        wfm.is_transient = True
        self.assertFalse(wfm.is_steady_state)
        self.assertTrue(wfm.is_transient)

        wfm = Waveform(np.zeros((48000,)), 48000, 0, is_steady_state=True)
        self.assertTrue(wfm.is_steady_state)
        self.assertFalse(wfm.is_transient)

        wfm = Waveform(np.zeros((48000,)), 48000, 0, is_steady_state=False)
        self.assertFalse(wfm.is_steady_state)
        self.assertTrue(wfm.is_transient)

    def test_impulse_AnalysisMethod_MIL_STD_1474E(self):
        from pytimbre.waveform import AnalysisMethod
        a_durations = [1e-4, 1e-3, 2.5e-3, 1e-2]
        peak = 165
        sample_rate = 200e3
        dur = 3.0
        bt = dur / 2
        for a_dur in a_durations:
            wfm = Waveform.generate_friedlander(
                peak_level=peak,
                a_duration=a_dur,
                duration=dur,
                sample_rate=sample_rate,
                blast_time=bt,
                noise=False
            )
            wfm.impulse_analysis_method = AnalysisMethod.MIL_STD_1474E

            # For MIL_STD_1474E, A_duration correction is on for only liaeq8hr value and SELA.
            self.assertAlmostEqual(wfm.SELA + 10 * np.log10(1 / (8 * 60 * 60)), wfm.liaeq8hr, delta=1e-2)

            # For MIL_STD_1474E, A_duration correction is on for only liaeq8hr value and SELA.
            self.assertAlmostEqual(wfm.liaeqT + 10 * np.log10(dur / 0.1), wfm.liaeq100ms, delta=1e-2)

            # A_duration correction
            a_duration_correction = - 1.5 * 10.0 * np.log10(wfm.corrected_a_duration / 2e-4)
            self.assertAlmostEqual(
                wfm.liaeq100ms + 10 * np.log10(0.1 / (8 * 60 * 60)) + a_duration_correction,
                wfm.liaeq8hr, delta=1e-2
            )

    def test_impulse_AnalysisMethod_MIL_STD_1474E_AFRL_PREF(self):
        a_durations = [1e-4, 1e-3, 2.5e-3, 1e-2]
        peak = 165
        sample_rate = 200e3
        dur = 3.0
        bt = dur / 2
        for a_dur in a_durations:
            wfm = Waveform.generate_friedlander(
                peak_level=peak,
                a_duration=a_dur,
                duration=dur,
                sample_rate=sample_rate,
                blast_time=bt,
                noise=False
            )
            wfm.impulse_analysis_method = AnalysisMethod.MIL_STD_1474E_AFRL_PREF

            liaeq8hr_w_corr = wfm.liaeq8hr
            a_duration_correction = - 1.5 * 10.0 * np.log10(wfm.corrected_a_duration / 2e-4)

            self.assertAlmostEqual(wfm.SELA + 10 * np.log10(1 / (8 * 60 * 60)), wfm.liaeq8hr, delta=1e-2)
            self.assertAlmostEqual(wfm.SELA + 10 * np.log10(1 / dur), wfm.liaeqT, delta=1e-2)
            self.assertAlmostEqual(wfm.SELA + 10 * np.log10(1 / .1), wfm.liaeq100ms, delta=1e-2)

            # A_duration correction check against no a_duration correction case
            wfm.impulse_analysis_method = AnalysisMethod.NO_A_DURATION_CORRECTIONS
            liaeq8hr_w0_corr = wfm.liaeq8hr
            self.assertAlmostEqual(liaeq8hr_w_corr, liaeq8hr_w0_corr + a_duration_correction, delta=1e-2)

    def test_impulse_AnalysisMethod_NO_A_DURATION_CORRECTIONS(self):
        # print()
        a_durations = [1e-4, 1e-3, 2.5e-3, 1e-2]
        peak = 165
        sample_rate = 200e3
        dur = 3.0
        bt = dur / 2
        for a_dur in a_durations:
            wfm = Waveform.generate_friedlander(
                peak_level=peak,
                a_duration=a_dur,
                duration=dur,
                sample_rate=sample_rate,
                blast_time=bt,
                noise=False
            )
            wfm.impulse_analysis_method = AnalysisMethod.NO_A_DURATION_CORRECTIONS

            self.assertAlmostEqual(wfm.SELA + 10 * np.log10(1 / (8 * 60 * 60)), wfm.liaeq8hr, delta=1e-2)
            self.assertAlmostEqual(wfm.SELA + 10 * np.log10(1 / dur), wfm.liaeqT, delta=1e-2)
            self.assertAlmostEqual(wfm.SELA + 10 * np.log10(1 / .1), wfm.liaeq100ms, delta=1e-2)

            self.assertAlmostEqual(wfm.corrected_a_duration, a_dur, delta=1.0e-2)
            # print("A_duration = {:.1f} ms - Dose = {:.2f} %".format(a_dur * 1000, results.noise_dose))

    def test_for_polarity_correction(self):
        wfm = Waveform.generate_friedlander(
            peak_level=-150,
            a_duration=2e-3,
            duration=3,
            sample_rate=200e3,
            blast_time=1.5,
            noise=True
        )

        # Flipping sign shouldn't affect the peak level property or the peak_difference method.
        positive_Iwfm = Waveform(pressures=wfm.samples,
                                 sample_rate=wfm.sample_rate,
                                 start_time=wfm.start_time,
                                 is_continuous_wfm=False)
        negative_Iwfm = Waveform(
            pressures=-1 * wfm.samples,
            sample_rate=wfm.sample_rate,
            start_time=wfm.start_time,
            is_continuous_wfm=False
        )

        self.assertAlmostEqual(positive_Iwfm.peak_level, negative_Iwfm.peak_level, delta=1e-7)
        self.assertTrue(positive_Iwfm.peak_level - negative_Iwfm.peak_level == 0.0)

    def test_get_features_eeg(self):
        """
        There appears to be an issue with the EEG data due to the low sample rate. So, here we will create a low
        sample rate waveform and attempt to get the auto-correlation data.
        """

        wfm = Waveform.generate_tone(50, 512, 300)
        wfm.window_size_seconds = 0.1
        wfm.hop_size_seconds = wfm.window_size_seconds / 10

        ac = wfm.auto_correlation

        self.assertEqual(12, ac.shape[1])
        self.assertEqual(30710, ac.shape[0])

    def test_split_by_time(self):
        """
        This test constructs a pure tone signal
        """
        frame_len = 0.25
        wfm = Waveform.generate_tone(frequency=250,
                                     sample_rate=4800,
                                     amplitude_db=94)
        wfms = wfm.split_by_time(frame_len)

        # Verify that the one-second signal results in 4 new waveforms for 0.25 int time
        self.assertEqual(len(wfms), 4)

        # Verify start times are correct for each new waveform
        t = 0
        for i in wfms:
            self.assertEqual(i.start_time, t)
            t = t + frame_len

        # Verify that the 1st sample in the last waveform is equal to the 3600th (4800*3/4) sample in the original
        # waveform
        self.assertEqual(wfm.samples[3600], wfms[3].samples[0])

        # Verify that the last sample in the first waveform is equal to the 1200-1 (4800*1/4-1) sample in the
        # original waveform
        self.assertEqual(wfm.samples[1199], wfms[0].samples[-1])

    def test_split_by_time_uneven(self):
        """
        This test constructs a pure tone signal
        """
        frame_len = 0.30
        wfm = Waveform.generate_tone(frequency=250,
                                     sample_rate=4800,
                                     amplitude_db=94)
        wfms = wfm.split_by_time(frame_len)

        # Verify that the one-second signal results in 3 new waveforms for 0.3 int time
        self.assertEqual(len(wfms), 3)

        # Verify start times are correct for each new waveform
        t = 0
        for i in wfms:
            self.assertEqual(i.start_time, t)
            t = t + frame_len
        # Verify that the last sample first frame is equal to 0.9*4800
        id = int(np.floor(0.6 * 4800))
        self.assertEqual(wfm.samples[id], wfms[2].samples[0])

    def test_specific_loudness(self):
        wfm = TestWaveform.filtering_waveform()
        single = np.array(
            [2.4008186338702053, 2.4008186338702053, 2.4008186338702053, 2.4008186338702053, 2.4008186338702053,
             2.4008186338702053, 2.4008186338702053, 2.4008186338702053, 11.726749296046957, 11.726749296046957,
             11.726749296046957, 11.726749296046957, 11.726749296046957, 11.726749296046957, 11.726749296046957,
             11.726749296046957, 11.726749296046957, 11.726749296046957, 11.5, 11.120000000000001, 10.740000000000002,
             10.360000000000001, 9.98, 9.600000000000003, 9.220000000000004, 9.0, 8.7, 8.4, 8.120000000000001, 7.84,
             7.56, 7.279999999999999, 6.999999999999999, 6.719999999999999, 6.440000000000001, 6.1, 5.909999999999999,
             5.719999999999999, 5.529999999999999, 5.34, 5.149999999999999, 4.959999999999997, 4.769999999999999,
             4.579999999999999, 4.4, 4.2700000000000005, 4.1400000000000015, 4.010000000000002, 3.880000000000002,
             3.7500000000000027, 3.620000000000003, 3.4900000000000038, 3.360000000000004, 3.2299999999999995, 3.1,
             2.9900000000000007, 2.880000000000001, 2.7700000000000014, 2.6600000000000015, 2.550000000000002,
             2.440000000000002, 2.3300000000000027, 2.2200000000000033, 2.13, 2.048, 1.9660000000000004,
             1.8840000000000008, 1.802000000000001, 1.7200000000000013, 1.6380000000000017, 1.5560000000000018,
             1.4740000000000022, 1.36, 1.2980000000000003, 1.2360000000000007, 1.1740000000000008, 1.112000000000001,
             1.0500000000000012, 0.9880000000000003, 0.9260000000000006, 0.8640000000000008, 0.82, 0.7780000000000001,
             0.7360000000000002, 0.6940000000000004, 0.6520000000000006, 0.6100000000000008, 0.5680000000000008,
             0.526000000000001, 0.48400000000000115, 0.4420000000000013, 0.4000000000000007, 0.42, 0.3980000000000001,
             0.3760000000000001, 0.3540000000000002, 0.3320000000000003, 0.3, 0.28300000000000003, 0.2660000000000001,
             0.24900000000000017, 0.2320000000000002, 0.22, 0.20900000000000005, 0.19800000000000006,
             0.1870000000000001, 0.17600000000000016, 0.1650000000000002, 0.15, 0.14200000000000002,
             0.13215526386710577, 0.13215526386710577, 0.13215526386710577, 0.13215526386710577, 0.13215526386710577,
             0.13215526386710577, 0.13215526386710577, 0.13215526386710577, 0.13215526386710577, 0.13215526386710577,
             0.13215526386710577, 0.13215526386710577, 0.13215526386710577, 0.12415526386710579, 0.11615526386710583,
             0.11153357640018147, 0.11153357640018147, 0.11153357640018147, 0.11153357640018147, 0.11153357640018147,
             0.11153357640018147, 0.11153357640018147, 0.11153357640018147, 0.11153357640018147, 0.11153357640018147,
             0.11153357640018147, 0.11153357640018147, 0.11153357640018147, 0.1, 0.09959381765986512,
             0.09959381765986512, 0.09959381765986512, 0.09959381765986512, 0.09959381765986512, 0.09959381765986512,
             0.09959381765986512, 0.09959381765986512, 0.09959381765986512, 0.09959381765986512, 0.09959381765986512,
             0.09959381765986512, 0.09959381765986512, 0.0924074058061083, 0.0924074058061083, 0.0924074058061083,
             0.0924074058061083, 0.0924074058061083, 0.0924074058061083, 0.0924074058061083, 0.0924074058061083,
             0.0924074058061083, 0.0924074058061083, 0.0924074058061083, 0.0924074058061083, 0.0924074058061083,
             0.0924074058061083, 0.0924074058061083, 0.08740740580610823, 0.08240740580610816, 0.07740740580610808,
             0.070943752416135, 0.070943752416135, 0.070943752416135, 0.070943752416135, 0.070943752416135,
             0.070943752416135, 0.070943752416135, 0.070943752416135, 0.070943752416135, 0.070943752416135,
             0.070943752416135, 0.06594375241613493, 0.06094375241613486, 0.055943752416134786, 0.05094375241613472,
             0.045943752416134645, 0.04218264471476026, 0.04218264471476026, 0.04218264471476026, 0.04218264471476026,
             0.04218264471476026, 0.04218264471476026, 0.04218264471476026, 0.035, 0.032999999999999974,
             0.030999999999999944, 0.028999999999999918, 0.02699999999999989, 0.024999999999999863,
             0.022999999999999833, 0.020999999999999804, 0.018999999999999774, 0.017919108947075064,
             0.017919108947075064, 0.017919108947075064, 0.017919108947075064, 0.015919108947075034,
             0.013919108947075006, 0.011919108947074979, 0.009919108947074949, 0.007919108947074921,
             0.0059191089470748935, 0.003919108947074864, 0.002824738108734737, 0.002824738108734737,
             0.002824738108734737, 0.002824738108734737, 0.002824738108734737, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
             0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0])

        specific_loudness_entire, specific_loudness_single = wfm.specific_loudness

        self.assertEqual(len(single), len(specific_loudness_single))

        for i in range(len(single)):
            self.assertAlmostEqual(single[i], specific_loudness_single[i], delta=1e-6)

        self.assertAlmostEqual(43.9, specific_loudness_entire, delta=1e-6)

    def test_boominess(self):
        self.assertTrue(True)
        # from timbral_models.Timbral_Booming import timbral_booming

        # wfm = TestWaveform.filtering_waveform()

        # boominess = timbral_booming(wfm.samples, wfm.sample_rate)

        # self.assertAlmostEqual(boominess, wfm.boominess, delta=1e-1)

    def test_integrated_loudness(self):
        wfm = TestWaveform.filtering_waveform()

        self.assertAlmostEqual(-1.845, wfm.integrated_loudness, delta=5e-2)

    def test_is_reverberant(self):
        wfm = Waveform.generate_tone(duration=5.0)
        self.assertFalse(wfm.is_reverberant)

        wfm = Waveform.generate_noise()
        self.assertFalse(wfm.is_reverberant)

        wfm = Waveform.generate_friedlander()
        self.assertFalse(wfm.is_reverberant)

    def test_roughness(self):
        wfm = Waveform.generate_tone()

        self.assertAlmostEqual(0, wfm.roughness, delta=1e-2)

        wfm = Waveform.generate_noise()
        self.assertTrue(wfm.roughness > 0)

    def test_sharpness(self):

        sharpness = np.array([31.793507160131632, 31.39454715817259, 37.78850002363926, 40.581824324942914,
                              38.45592158530506, 43.67976264104787, 44.707132703380935, 42.00172246724979,
                              46.08900328959798, 50.17865153179774, 47.276854728334584, 52.415544079022794,
                              50.1541299534912, 50.121582644009145, 55.05548801461816, 56.24412156817093,
                              54.339136978844195, 57.617549991223484, 56.35695212799798, 55.8857323336745,
                              58.33508763370355, 59.52333062514441, 61.12828170497457, 65.6140159278383,
                              69.4448034580627, 69.83404497484568, 71.09062107330647, 70.49623890524276,
                              69.60796120530836, 69.02225013727094, 70.63238393204617, 72.6989990580359,
                              75.53337438739166, 79.62536088657404, 79.12252335296324, 81.1614780588346,
                              81.16423606341046, 78.93431210342833, 80.61634736064119, 82.22500290413765,
                              83.57512128176248, 83.28740108177206, 83.29900679740604, 82.65193294756179,
                              85.13355173737102, 86.27998778736048, 85.86954289600844, 87.82612442893027,
                              88.05765955151598, 88.70281715947723, 89.98153139251721, 92.41976088478752,
                              95.69567290081969, 98.18600096575318, 99.79905715206688, 99.23772208239869,
                              100.50594287243207, 100.54875805536338, 100.2180571017063, 99.74835451924646,
                              99.16624982624649, 97.99193205906337, 97.60661525866901, 97.656128989844,
                              95.32282466779023, 96.2142024270529, 96.92599552182946, 95.85790580216408,
                              98.20583872208292, 100.37968487083714, 104.29297062130019, 106.92097170006029,
                              106.26676809777764, 104.47505457413067, 108.46507743321473, 109.16707985084287,
                              107.79599445502383, 109.08130799324908, 104.00430746874525, 103.06910009581283,
                              105.4044781473893, 106.60101508896582, 107.68790254476025, 108.47144733608091,
                              108.49994935615803, 107.34469456085667, 105.17980544246063, 101.70901909453643,
                              100.72328616085188, 99.9826081108301, 99.35676778021985, 98.33464079838923,
                              98.51612546476305, 103.08866159576141, 104.50193328309025])

        n = 0
        for f in range(500, 10000, 100):
            wfm = Waveform.generate_tone(frequency=f)
            self.assertAlmostEqual(sharpness[n], wfm.sharpness, delta=5e-1)
            n += 1

    def test_pad(self):
        wfm = Waveform.generate_tone()

        self.assertEqual(1.0, wfm.duration)

        wfm2 = wfm.pad([10, 0], [0, 0])
        self.assertEqual(wfm.sample_rate + 10, len(wfm2.samples))

        for i in range(10):
            self.assertAlmostEqual(0, wfm2.samples[i], delta=1e-12)

        wfm2 = wfm.pad(10, 0)
        self.assertEqual(wfm.sample_rate + 20, len(wfm2.samples))

        for i in range(10):
            self.assertAlmostEqual(0, wfm2.samples[i], delta=1e-12)
            self.assertAlmostEqual(0, wfm2.samples[-i], delta=1e-12)

    def test_overall_level(self):
        wfm = Waveform.generate_tone()

        lz = wfm.overall_level()
        la = wfm.overall_level(weighting=WeightingFunctions.a_weighted)

        self.assertNotEqual(lz[0], la[0])

    def test_plot_waveform(self):
        pass

    def test_get_waveform_subset(self):
        #   Read the file
        sbf = StandardBinaryFile(self.test_bin_write())
        self.assertFalse(np.any(np.isnan(sbf.samples)))

        #   Create a new waveform that is trimmed from this data
        subset1 = sbf.trim(0, 1000)
        self.assertFalse(np.any(np.isnan(subset1.samples)))

        #   Since the samples are automatically adjusted for DC offset, we must complete that again here.
        subset1._samples -= np.mean(subset1.samples)

        #   Create a new waveform by not reading all the data and then trimming
        subset2 = StandardBinaryFile(self.test_bin_write(), s0=0, s1=1000)
        self.assertFalse(np.any(np.isnan(subset2.samples)))
        self.assertEqual(subset2.duration, subset1.duration)
        self.assertEqual(subset2.start_time, subset1.start_time)
        for i in range(len(subset2.samples)):
            self.assertAlmostEqual(subset2.samples[i], subset1.samples[i], delta=1e-10)

        #   Do the same thing, but this time make it a non-zero starting sample
        s0 = int(1)
        s1 = int(1001)
        subset1 = sbf.trim(s0, s1)
        self.assertFalse(np.any(np.isnan(subset1.samples)))
        subset1._samples -= np.mean(subset1.samples)
        subset2 = StandardBinaryFile(self.test_bin_write(), s0=s0, s1=s1)
        self.assertFalse(np.any(np.isnan(subset2.samples)))
        self.assertEqual(subset2.duration, subset1.duration)
        self.assertEqual(subset2.start_time, subset1.start_time)
        for i in range(len(subset2.samples)):
            self.assertAlmostEqual(subset2.samples[i], subset1.samples[i], delta=1e-10)

    def test_get_subset(self):
        sbf = StandardBinaryFile(self.test_bin_write())
        self.assertFalse(np.any(np.isnan(sbf.samples)))

        s0 = 0
        s1 = 1000
        subset1 = sbf.trim(s0, s1)
        subset1._samples -= np.mean(subset1.samples)
        subset2 = StandardBinaryFile(self.test_bin_write(), s0=s0, s1=s1)
        self.assertEqual(subset2.duration, subset1.duration)
        self.assertEqual(subset2.start_time, subset1.start_time)
        for i in range(len(subset1.samples)):
            self.assertAlmostEqual(subset2.samples[i], subset1.samples[i], delta=1e-10)

        s0 = 1
        s1 = 1000
        subset1 = sbf.trim(s0, s1)
        subset1._samples -= np.mean(subset1.samples)
        subset2 = StandardBinaryFile(self.test_bin_write(), s0=s0, s1=s1)
        self.assertEqual(subset2.duration, subset1.duration)
        self.assertEqual(subset2.start_time, subset1.start_time)
        for i in range(len(subset1.samples)):
            self.assertAlmostEqual(subset2.samples[i], subset1.samples[i], delta=1e-10)

    def test_from_StandardBinaryFile(self):
        filename = str(Path(__file__).parents[0]) + "/Test Data/audio_files/test_header_format.bin"

        sbf = Waveform.from_StandardBinaryFile(filename,
                                               start_time_key="START TIME",
                                               sample_format_key="MACHINE FORMAT")

        self.assertEqual(1484800, len(sbf.samples))
        self.assertTrue(isinstance(sbf.start_time, datetime.datetime))
        self.assertEqual(2020, sbf.start_time.year)
        self.assertEqual(1, sbf.start_time.month)
        self.assertEqual(15, sbf.start_time.day)
        self.assertEqual(21, sbf.start_time.hour)
        self.assertEqual(49, sbf.start_time.minute)
        self.assertEqual(45, sbf.start_time.second)
        self.assertEqual(0.088e6, sbf.start_time.microsecond)
        self.assertEqual(22, len(sbf.header))

    def test_to_StandardBinaryFile(self):
        import os.path

        filename = str(Path(__file__).parents[0]) + "/Test Data/audio_files/test_to_standardBinaryFile.bin"
        if os.path.exists(filename):
            os.remove(filename)

        #   Create a new waveform
        wfm = Waveform.generate_tone()

        #   Add a header entry
        wfm.header['test entry'] = 'testing post-hoc data addition'

        #   Write the data to a StandardBinaryFile format and load it back in
        wfm.to_StandardBinaryFile(filename)
        self.assertTrue(os.path.exists(filename))
        sbf = Waveform.from_StandardBinaryFile(filename)

        self.assertEqual(6, len(sbf.header))
        self.assertEqual(wfm.duration, sbf.duration)

        for i in range(len(wfm.samples)):
            self.assertAlmostEqual(wfm.samples[i], sbf.samples[i], delta=1e-5)


class TestFrameBuilder(unittest.TestCase):
    def test_complete_frame_count(self):
        wfm = Waveform.generate_tone(duration=2)

        builder = FrameBuilder(wfm=wfm, overlap_pct=0.75, frame_width_sec=1)

        self.assertEqual(2, builder.duration)
        self.assertEqual(1, builder.frame_length_seconds)
        self.assertEqual(48000, builder.frame_length_samples)
        self.assertEqual(0.25, builder.time_increment)
        self.assertEqual(0.75, builder.excess_duration)
        self.assertEqual(5, builder.complete_frame_count)

    def test_get_next_frame(self):
        #   Define a waveform with a 94 dB and 114 dB amplitude
        wfm94 = Waveform.generate_tone()
        wfm114 = Waveform.generate_tone(amplitude_db=114)

        #   Now create a new waveform that is the concatenation of the two signals
        samples = np.concatenate([wfm94.samples, wfm114.samples])
        wfm = Waveform(pressures=samples, sample_rate=wfm94.sample_rate, start_time=0)

        #   Create a FrameBuilder with 50% overlap and test the waveforms
        builder = FrameBuilder(wfm=wfm, overlap_pct=0.5, frame_width_sec=1)
        self.assertEqual(3, builder.complete_frame_count)
        w1 = 1.0
        w2 = 0.0
        for i in range(builder.complete_frame_count):
            lvl = 10*np.log10(w1*10.0**(94/10)+w2*10**(114/10))
            self.assertAlmostEqual(lvl, builder.get_next_waveform_subset().overall_level(), delta=1e-5)

            w1 -= builder.overlap_percentage
            w2 += builder.overlap_percentage

        #   Create a FrameBuilder with 50% overlap and test the waveforms
        builder = FrameBuilder(wfm=wfm, overlap_pct=0.75, frame_width_sec=1)
        self.assertEqual(5, builder.complete_frame_count)
        w1 = 1.0
        w2 = 0.0
        for i in range(builder.complete_frame_count):
            lvl = 10*np.log10(w1*10.0**(94/10)+w2*10**(114/10))
            self.assertAlmostEqual(lvl, builder.get_next_waveform_subset().overall_level(), delta=1e-5)

            w1 -= 1-builder.overlap_percentage
            w2 += 1-builder.overlap_percentage



