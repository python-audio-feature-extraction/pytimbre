PyTimbre - Development History
==============================

**2020**

*April*

- Added logic to avoid adding the STFT when it is not within the input configuration
- Added logic to generate and evaluate functions to skip over elements that are not included, or included as None
- Added try catch to trap error on linear algebra analysis
- Added a new function to create a column-wise output file for the results.
- Edited the calculation of the Attack so that if it returns a single value rather than the expected array, it will continue to process the data.
- Added logic to put -9999 in place of the NaN while exporting data that was not calculated

*May*

- Added logic to ensure that the index is always less than the length of the vector (lines 967 of StaticMethods)

*July*

- Added the History.md file back into the repository, and changed the column widths in every file to the limit within PyCharm.

*October*

- Added comments and details regarding classes.

**2021**

*May*

- Added the average statistic to the output dictionary

- Added logic to the determination of the Attack start and stop in cases when there is no array matching the constraints.

*June*

- Added functions to SoundFile to facilitate easier use of the class.  Created an 'all' and new 'default' configuration.  Added the ability to return only a specific metric from the new 'process' function.
- Updated calculation within Harmonic representation to address potential for index violations.

*July*

- Updated the default_configuration function
- Updated document string and other formatting issues within the source code.
- Initial design of new interface complete.  This design removes the MATLAB style 'eval' command and creates the parent-child classes for the various spectro-temporal representations.  The entirety of the feature extraction is contained within the parent class and provides the interface for interaction with the pytimbre classes.

*August*

- Added a function to the AudioSignal that permits the definition of the names of the features prior to running the analysis.  This assists with the definition of the DataFrame that might hold the data.

**2022**

*May*

- Added a refactored version of the generic_time_waveform object from external sources. This provides the basis for calculating the features on the waveform
- Added a refactored version of the wav_file class that permits the reading/writing of most wav files that have canonical format. This class also includes the ability to read/write additional chunks for the addition of meta-data to the wave file.

*June*

- Updated the interface with the deprecation module. This permits the inclusion of the former methods, but marks them with deprectation decoration.
- Added tests for the various new methods for representation of the temporal and spectral characteristics.
- Updated the Data_Chunk to return a single dimension vector when reading a mono wav file.
- Added functions to create the dictionary of featres from a Spectrum object
- Numerous minor fixes to ensure the functioning of the system for the calculation of the various features

*August*

- Added files from another library to provide test cases for the various shared classes
- Updated the spectrum file with a spectrogram class that creates a short time Fourier transform in a manner similar to the spectrogram function from scipy but in accordance with the Matlab Timbre toolbox code.
- Removed the time history calculation ability
- Moved the chunk classes that support the WaveFile class to the same file.
- Create test cases for the single frequency sine wave. This test case was pushed through the MATLAB version of the toolbox. All spectral features, but the spectral variance, were checked against these results.

**2023**

*January*

- Fixed use of automatic headers on history and readme files.
- Added a property to the spectrogram class that returns a dictionary of the various temporal and spectral features, averaged in time.
- Began adding the hooks for inclusion of the mosqito sound quality metrics for extension of the auditory features into this realm.
- Added acoustic weighting and fractional octave band classes to begin facilitation of the inclusion of sound pressure level determination based on FFT and digital filters.
- Added elements of other AFRL spectral calculations in preparation for an increase of the minor revision number. 
- Renamed the spectrum.py and time_history.py files to their plural since they contain multiple classes.

*February*

- There was an issue reading StandardBinaryFile formatted files from another organization. A significant amount of 
  restructuring was conducted on the constructor of this class to permit multiple variations on the required data 
  within the header.
- The get_features function in the Spectrum class did not return all the values of the spectral features in the 
  dictionary. This was updated and a new version pushed.
- Added a function to wrap up the creation of the generic time history with fractional octave resolution calculated from the Narrowband spectra
- Edited the TimeHistory and NarrowbandTimeHistory classes to insert the header, correctly set the integration time on loading the file, and run new tests.
- Changed how WaveFile.audio_info returns the data
- Created a property to obtain the time and spectral Timbre features, sound quality metrics, and level values for a given TimeHistory

*March*

- Implemented function to scale current waveform based on a calibration signal passed to a function
- Implemented the various forms of equivalent level calculations
- Implemented clipping detection using an external dependency
- Updated the information within the WaveFile to support writing multi-channel audio files to disk
- Re-incorporated classes to read audio files generated as a pre-cursor to the StandardBinaryFile

*April*

- Version 0.6.4
- Version 0.6.3 integrated the libf0 library. After attempting to integrate this library directly, it was determined that using the raw code would be a better implementation for this package.
- The swipe.py file was taken from version 1.0.2 on 1 April 2023 to provide the fundamental frequency
- Version 0.6.7
- Updates to the calculation of the fundamental frequency
- Updates to the calculation of the harmonic features
- Updates to methods to get the features from the spectrum and time history objects

*May*

- Added a flag in the constructor of the waveform to remove the finding the maximum of the waveform
- Created the initial version of the Sphinx documentation for the module.
- Integrated code for the reading of impulse waveforms into the Waveform class.
- Integrated code the correct calculation of spectral levels from an impulse waveform into SpectrumByFFT
- Updated the **get_features** function to return different data in the DataFrame when the waveform is labeled as an impulse waveform.

*June*

- Updated trim method to copy properties that could have been altered from the defaults in the constructor
- Reduced requirements for standard packages
- Worked on temporal features for EEG data that possesses significantly lower sample rates than audio data
- Added a property to set the method for the thresholding required in the attack determination
- Updated the tests so that they will execute with python setup.py test
- Incremented to 0.7.1 with the completion of all issues slated for the 0.7.1 increment.
- Incremented to 0.7.2
- Added spectrogram class that uses the spectrogram function in scipy.signal to create the matrix of sound pressure levels
- Added a plotting function to the time history class.

*July*

- Added speech features from python_speech_features
- Fixed errors in start_time function within WaveFile class

*August*

- Cleaned up some code in Waveform
- Added a split_by_time function that mimics the split function in Numpy
- Updated the calculation of single number metrics. Some were calculated in frequency spectra, others on time windows. To compensate for this, a new temporal metrics namespace was created. Tests were added and elements of the acoustic weights namespace were moved because the are more appropriately in the temporal metric namespace
- Added code to permit the reading of Wavefiles that do not have a LIST chunk, or do not have a 'creation_date' element in the LIST chunk
- Cleaned up some Docustrings.
- Integrated many of the functions from the timbral_models to enable better calculation of the various sound quality metrics that exist.
- With the integration of the timbral_models, the dependency on mosqito was removed

*November*

- Updated the function to find the attack region of the waveform to be more robust and try different methods if the first method fails.
- Incremented the version 0.8.1

*December*

- Updated the functions to locate the various attack elements for the processing of speech stimuli under Holistic Models of Decision-making Line of Effort program.

**2024**

*February*

- Implemented a Padding function using the Numpy.Pad function.

*March*

- Corrected the _is_calibration_ function to return values when the wave file is more than single channel.

*June*

- The reading of the wav files returned an absolute level that was half of what was shown in Audacity. This was determined to be the issue where the maximum value of the integer was assumed to be 2**32 -1. However, the correct representation of this would have been 2**(32-1) which is correctly evaluated by np.iinfo(np.int32).max. All integer interpretations for the maximum permitted value were adjusted with the Numpy function.
- Reduced the Python requirements to 3.8 to facilitate usage by Windows 7 computer.
- Added interpolation and trim functions to TimeHistory class
- Closed a number of issues.
- Updated the from_data function, and updated the uses of it within the test cases.
- Added function to plot the waveform

*July*

- Updated the methods that are used to generate the subsets of the waveform within the TimeHistory classes. This will use the FrameBuilder to class to make the Waveform that represents the element that is processed with the spectrum object.
- Updated the version to 0.9.1 by elevating the two remaining issues to the next milestone.

*October*

- Updated logic error in obtaining acoustic features
- Implemented a new error when the upper frequency of a digital filter exceeds the Nyquist frequency
- Added a test case for an audio deep fake
- Updated all creation of Waveform objects within the Waveform class to ignore the remove DC offset. This means that the user is responsible for the cleaning of the samples. This is within the dependent functions, like resample, where a new Waveform is created. It is assumed that the user will have already done whatever is desired before doing these dependent functions.
- Removed the references to the Cochleargram