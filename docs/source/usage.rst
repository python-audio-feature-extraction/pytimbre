Usage
=====

Installation
------------

To use PyTimbre, first install it using pip:

.. code-block::

    $ pip install pytimbre

Defining a waveform from an array of values
-------------------------------------------

.. code-block::

    from pytimbre.waveform import Waveform

    fs = 48000
    w = 2 * np.pi * f
    t = np.arange(0, 10, 1/fs)

    wfm = Waveform(0.75 * np.sin(w*t), fs, 0.0)

Define a waveform from a wav file
---------------------------------

.. code-block:: console

    from pytimbre.audio_files.wavefile import WaveFile
    wfm = wave_file(filename)

Obtain global temporal attributes
---------------------------------

.. code-block:: console

    from pytimbre.audio_files.wavefile import WaveFile

    wfm = WaveFile(filename)
    print(wfm.amplitude_modulation)

Create single spectrogram and get a feature
-------------------------------------------

.. code-block:: console

    from pytimbre.audio_files.wavefile import WaveFile
    from pytimbre.spectra import SpectrumByFFT

    wfm = wave_file(filename)
    spectrum = SpectrumByFFT(wfm)
    print(spectrum.spectral_roll_off)
