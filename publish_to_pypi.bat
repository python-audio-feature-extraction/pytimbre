ECHO OFF

rem
rem
REM We will do a little clean-up just to make sure that the only files uploaded are the most recent
rem
rem

rd build /s
rd dist /s

REM
REM
rem	This file is a list of the command line commands that need to be executed to 
REM publish a python module to PYPI. This is created from the information here:
REM https://towardsdatascience.com/how-to-publish-a-python-package-to-pypi-7be9dd5d6dcd
REM and requires that the command line be able to access Python and run in 
REM administrator mode.
REM
REM
rem Create the distribution that we want to publish

python setup.py sdist bdist_wheel

REM 
REM Upload the data to the TestPyPi.org site
REM

REM python -m twine upload --repository testpypi dist/*

REM pip install -i https://test.pypi.org/pytimbre/ pytimbre==0.5.1

REM
REM Now upload to the real Pypi site
REM

python -m twine upload dist/*

REM
REM Now we can run the sphinx build for the documents
REM

sphinx-build -b html docs/source/ docs/build/html